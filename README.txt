*****This game is in a pre-alpha stage. This means the gameplay, including all of it's elements, are subject to change.*****


This game, codename: Kinzoku[: Tower of Trial], is being developed on the Solarus Engine which was specfically by and for fans of the Legend
of Zelda series; specifically the third installment, A Link to the Past (ALttP) for the Super Nintendo. This was one of my favorite games as a kid 
(and still today) so this game is done in a similar style and currently uses sound effects from ALttP as placeholders until I have original sound
and music. I have been working with the Solarus Engine for just over two years now. Most of that time has been spent teaching myself how to use 
the Engine. About six months ago I started developing this game. Every day is a learning process as I put all the pieces together to progress to 
the next step. This step that I am taking, to give what I have made to others to use, is not something I am generally confident in but I feel that
I have put together the foundations for a game that will have fun challenges with high replayability. I am very interested to hear comments and 
criticisms; please email brightmatterstudios@gmail.com. Thank you for playing my game and supporting me.

--Brightmattershiek


----------------------------------------------------------------------------------------------------------------------------------------------
OBJECTIVE:
----------
The objective of the game is to enter the tower and defeat all the enemies. When you enter the tower, the door will lock behind you.
Once you defeat all three waves of enemies and the Boss, the door to the next floor will open. Currently there are only two floors and
very little in the way of 'extras', such as alternate weapons or items of any kind; although, there is a strange creature on the second floor...

LAUNCHING THE GAME
------------------
Simply run the 'installer' to bring up the Quest Launcher. You should see the game in the Quest Panel and to the right is a small button labelled 'Play'.
Click that and you will be good to go! 

If for some reason this doesn't work, you must manually add the Quest to the Launcher. Click the Green button at the bottom of the Quest Panel to add
a new Quest. Navigate to the folder you installed the game. You are looking for a file called quest.dat and it should be installed at 
C:/Program Files/Kinzoku/data. If it is at that location, you shouldn't even need to be reading this far down. Anyway, if there are any issues please 
email brightmatterstudios@gmail.com.


-----------------------------------------------------------------------------------------------------------------------------------------------
CONTROLS
--------


By default, the control scheme is as follows:

DEFAULT CONTROLS:
-----------------

Up = W
Down = S
Left = A
Right = D
Attack = Left Mouse Button (LMB)
Action/Interact = Space
Pause = Escape

NOTE: WASD and Space are used for navigating and selecting text as well.

If you would like a different control scheme (specifically, one more suited to a keyboard/buttons-only playstyle), start the game, press F4,
and type cc. Then press enter and F4 to close the menu. To return to the default simply repeat the process and type dc instead of cc.
This will change the controls to the following:

CLASSIC CONTROLS:
-----------------

Up = Up Arrow
Down = Down Arrow
Left = Left Arrow
Right = Right Arrow
Attack = C
Action/Interact = Space
Pause = D

FULLSCREEN
----------

By default the game is in fullscreen. If you would like to change this, simply press F11 to toggle between fullscreen and windowed.
NOTE: If you play the game in windowed mode, use caution where you click (if you are using the default controls). If the mouse is 
outside the window, you will switch windows if you click. 