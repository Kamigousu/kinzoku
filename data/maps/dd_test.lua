-- Lua script of map dd_test.
-- This script is executed every time the hero enters this map.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation:
-- http://www.solarus-games.org/doc/latest

local map = ...
local game = map:get_game()


function kalmar:on_interaction()

  if game:get_value("kalmar_dialog") == nil then
    game:start_dialog("kalmar", function(answer)  
      if answer == 3 then --Say nothing.
        game:set_value("kalmar_dialog", 1)
      elseif answer == 4 then --Tell your story.
        game:set_value("kalmar_dialog", 2)
      end
      game:start_dialog("kalmar."..game:get_value("kalmar_dialog"), function(answer)
        if game:get_value("kalmar_dialog") == 1 then
          if answer == 3 then --Say nothing.
            game:set_value("kalmar_dialog", 3)
          elseif answer == 4 then --Tell your story.
            game:set_value("kalmar_dialog", 2)
          end
          game:start_dialog("kalmar."..game:get_value("kalmar_dialog"), function()
            game:set_value("kalmar_dialog", 3)
          end)
        end
      end)
    end)
  end
  if game:get_value("kalmar_dialog") == 3 then
    game:start_dialog("kalmar.3")
  end
end