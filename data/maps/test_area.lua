-- Lua script of map test_area.
-- This script is executed every time the hero enters this map.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation:
-- http://www.solarus-games.org/doc/latest

local map = ...
local game = map:get_game()

-- Event called at initialization time, as soon as this map is loaded.
--[[function map:on_started()
local hero = self:get_hero()
--sol.timer.start(hero, 10, function() hero:face_cursor() return true end)
sol.audio.set_music_volume(65)
  -- You can initialize the movement and sprites of various
  -- map entities here.

print(game:get_command_joypad_binding("up"))
print(game:get_command_joypad_binding("down"))
print(game:get_command_joypad_binding("left"))
print(game:get_command_joypad_binding("right"))
end
--]]

local function start_shop()
local table = require("scripts/menus/shop")
game:set_value("shop", true)  --True means the shop is started and displayed on screen.
sol.menu.start(map, table)  --Start the lua script containing the shop table/menu.
end

function shop:on_interaction()
  local shop_dialog = game:get_value("shop_dialog")
  
  if shop_dialog == nil then
    game:start_dialog("shop.greeting", function(answer)
                      if answer == 2 then --Talk to shop.
                        game:start_dialog("shop.talk")
                        game:set_value("shop_dialog", 2)
                      elseif answer == 3 then  --Start sale screen.
                        game:start_dialog("shop.sale", function() start_shop() end)
                        game:set_value("shop_dialog", 3)
                      else  --Answer == 4; Leave the shop.
                        game:start_dialog("shop.goodbye")
                        game:set_value("shop_dialog", 4)
                      end
                    end)
  else
    game:start_dialog("shop.resume", function(answer)
                      if answer == 2 then --Talk to shop.
                        game:start_dialog("shop.talk")
                      elseif answer == 3 then  --Start sale screen.
                        game:start_dialog("shop.sale", function() start_shop() end)
                      else  --Answer == 4; Leave the shop.
                        game:start_dialog("shop.goodbye")
                      end
                    end)
  end
end
