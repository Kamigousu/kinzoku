-- Lua script of map floor_1.
-- This script is executed every time the hero enters this map.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation:
-- http://www.solarus-games.org/doc/latest

local map = ...
local game = map:get_game()


function map:on_draw()
    map:start_overlay("hero_light")
end


function start_cd_f1:on_activated()   --sensor for activating waves after player walks in.
  if not game:get_value("floor_1_clear") then
    sol.timer.start(map, 5000, function() 
                               map:get_entity("to_outside"):set_enabled()
                               map:get_entity("outside_light"):set_enabled(false)
                               map:get_entity("outside_light_long"):set_enabled(false) 
                               map:start_wave(1)
                               sol.timer.start(map, 100, function() map:check_wave() return true end)
                               return false end)
  end
end

