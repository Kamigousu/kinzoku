-- Lua script of item potion-health.
-- This script is executed only once for the whole game.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation for the full specification
-- of types, events and methods:
-- http://www.solarus-games.org/doc/latest

local item = ...
local game = item:get_game()
local hero = game:get_hero()

--Amount of health restored by using the potion.
local amt
local variant

-- Event called when the game is initialized.
-- Initialize the properties of your item here,
-- like whether it can be saved, whether it has an amount
-- and whether it can be assigned.
function item:on_started()

  item:set_shadow(nil)
  item:set_assignable()
  item:set_obtainable()
  item.is_stackable = true
  item.max_amount = 10
  item:set_savegame_variable("blood_wine")
  item:set_amount_savegame_variable("blood_wine_amt")
  item:set_brandish_when_picked(false)

end


-- Event called when the hero is using this item.
-- Define here what happens when using this item
-- and call item:set_finished() to release the hero when you have finished.
function item:on_using()

  amt = math.ceil(game:get_max_life() / 3)

  if game:get_life() ~= game:get_max_life() then

    for i = 1, amt do
      game:add_life(1)
  
    end

  end
  game:remove_from_current_inventory(item)
  item:set_finished()
end

-- Event called when a pickable treasure representing this item
-- is created on the map.
function item:on_pickable_created(pickable)

end
