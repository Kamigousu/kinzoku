-- Lua script of item dried_meat.
-- This script is executed only once for the whole game.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation for the full specification
-- of types, events and methods:
-- http://www.solarus-games.org/doc/latest

local item = ...
local game = item:get_game()
local hero = game:get_hero()

-- Event called when all items have been created.
function item:on_started()
  -- Initialize the properties of your item here,
  -- like whether it can be saved, whether it has an amount
  -- and whether it can be assigned.

  item:set_shadow(nil)
  item:set_assignable()
  item:set_obtainable()
  item.is_stackable = true
  item.max_amount = 5
  item:set_savegame_variable("dried_meat")
  item:set_amount_savegame_variable("dried_meat_amt")
  item:set_brandish_when_picked(false)

end

-- Event called when the hero starts using this item.
function item:on_using()
  -- Define here what happens when using this item
  -- and call item:set_finished() to release the hero when you have finished.
  local count = 0
  local dried_meat_timer = sol.timer.start(game, 1250, function()  
    local health
    local max_health = game:get_max_life()
    local amt = 1

    count = count + 1
    game:add_life(amt)

    health = game:get_life()
print(count)
    if count >= 48 or health >= max_health then count = 0 return false else return true end

  end)

  game:remove_from_current_inventory(item)
  item:set_finished()
end

-- Event called when a pickable treasure representing this item
-- is created on the map.
function item:on_pickable_created(pickable)

  -- You can set a particular movement here if you don't like the default one.
end
