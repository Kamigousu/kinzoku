-- Lua script of item sickleleaf_tea.
-- This script is executed only once for the whole game.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation for the full specification
-- of types, events and methods:
-- http://www.solarus-games.org/doc/latest

local item = ...
local game = item:get_game()
local hero = game:get_hero()

-- Event called when the game is initialized.
function item:on_started()
  -- Initialize the properties of your item here,
  -- like whether it can be saved, whether it has an amount
  -- and whether it can be assigned.

  item:set_shadow(nil)
  item:set_assignable()
  item:set_obtainable()
  item.is_stackable = true
  item.max_amount = 5
  item:set_savegame_variable("sickle_tea")
  item:set_amount_savegame_variable("sickle_tea_amt")
  item:set_brandish_when_picked(false)

end

-- Event called when the hero is using this item.
function item:on_using()
  -- Define here what happens when using this item
  -- and call item:set_finished() to release the hero when you have finished.

  local stamina = hero:get_stamina()
  local max_stamina = hero:get_max_stamina()
  local missing_stamina = (max_stamina - stamina)*(0.35)
  local stamina_15 = max_stamina*(0.15)
  local amount

  if missing_stamina > stamina_15 then
    amount = missing_stamina
  else
    amount = stamina_15
  end

  for i = 1, amount do
    hero:add_stamina(1)
  end

  game:remove_from_current_inventory(item)
  item:set_finished()
end

-- Event called when a pickable treasure representing this item
-- is created on the map.
function item:on_pickable_created(pickable)

  -- You can set a particular movement here if you don't like the default one.
end
