-- Lua script of item airlash.
-- This script is executed only once for the whole game.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation for the full specification
-- of types, events and methods:
-- http://www.solarus-games.org/doc/latest

local item = ...
local game = item:get_game()
local hero = game:get_hero()

-- Event called when all items have been created.
function item:on_started()
  -- Initialize the properties of your item here,
  -- like whether it can be saved, whether it has an amount
  -- and whether it can be assigned.

  item:set_shadow(nil)
  item:set_assignable()
  item:set_obtainable()
  item.is_stackable = true
  item.max_amount = 3
  item:set_savegame_variable("featherweight")
  item:set_amount_savegame_variable("featherweight_amt")
  item:set_brandish_when_picked(false)

end

-- Event called when the hero starts using this item.
function item:on_using()
  -- Define here what happens when using this item
  -- and call item:set_finished() to release the hero when you have finished.
  hero = game:get_hero()
  game:set_value("featherweight_buff", true)
  hero:calculate_parameters()
--print("buffs/debuffs set true")
  local obsidian_razor_timer = sol.timer.start(game, 45000, function()  
    game:set_value("featherweight_buff", false)
    hero:calculate_parameters()
--print("buffs/debuffs false")
    return false
  end)

  game:remove_from_current_inventory(item)
  item:set_finished()
end

-- Event called when a pickable treasure representing this item
-- is created on the map.
function item:on_pickable_created(pickable)

  -- You can set a particular movement here if you don't like the default one.
end
