-- Lua script of enemy template.
-- This script is executed every time an enemy with this model is created.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation for the full specification
-- of types, events and methods:
-- http://www.solarus-games.org/doc/latest

local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()
local sprite
local movement

--enemy parameters
local grin = false
local invisible = false
local bonus_drop
local opacity
local tier = 1  --This is the 'power level' of the enemy. Higher tiers are more powerful; 0/1 is the lowest tier.
local life = 50
local damage = math.random(8, 15)  --Damage is random for each enemy.
local invis_chance = 35 --Used as a percentage.
local detection_long = 300  --Maximum distance the hero can be detected from.
local detection_short = 200  --Pathfinding movement cannot exceed a distance of 200 pixels or it will cause unpredictable problems.
--local exp --Unnecessary until experience is used.
--local base_exp = 5  --Unnecessary until experience is used.
local has_seen_hero = false --This enemy has not seen the hero yet, by default.
local state
local los
local last_anim
local loot = enemy:set_treasure(enemy:get_loot(tier, floor))  --This is the reward the enemy will drop upon death.


-- Event called when the enemy is initialized.
function enemy:on_created()

  -- Initialize the properties of your enemy here,
  -- like the sprite, the life and the damage.
  sprite = enemy:create_sprite("enemies/" .. enemy:get_breed())
  enemy:set_life(life)
  enemy:set_damage(damage)
  opacity = sprite:get_opacity()
end

-- Event called when the enemy should start or restart its movements.
-- This is called for example after the enemy is created or after
-- it was hurt or immobilized.
function enemy:on_restarted()
  local hero_dist = enemy:get_distance(hero)
  local dir = sprite:get_direction()
  local hero_dir = enemy:get_direction4_to(hero)
  los = enemy:has_los(hero)

  --enemy:check_hero()
  if inivisible then enemy:reappear() end

  if hero_dist <= detection_long and dir ~= hero_dir then
    enemy:face_hero() 
  end

--Use the grinning behaviour if enemy is grinning.
  if grin then
    local rand = math.random(1, 100)
    if rand <= invis_chance then
      enemy:invisible()
    else
      enemy:stop()
    end
--Else use the standard (non-grinning) behaviour.
  elseif not grin then

    if hero_dist <= detection_long and dir == hero_dir and los then
      enemy:grin()
    end

    if not los or hero_dist > detection_long then
      enemy:stop()
    end
  end
end

--Stop the enemy and start wandering after a short period.
function enemy:stop()
  local rand_num = math.random(250, 1250) --Random time in ms between .25 and 1.25 seconds.

  los = enemy:has_los(hero)
  if los then rand = 50 end  --If enemy has los, finish the timer faster than normal.

  if movement ~= nil then movement:stop() end
  state = "stopped"
  if grin then
    sprite:set_animation("stopped_grin")
  else
    sprite:set_animation("stopped")
  end
  sol.timer.start(enemy, rand_num, function() if grin and los and enemy:get_distance(hero) <= detection_short then enemy:find_hero() else enemy:wander() end return false end)
end

function enemy:wander()

  local rand = math.random(750, 1250)

  los = enemy:has_los(hero)
  if los and not invisible then rand = 50 end --If the enemy has los, finish the timer faster than normal.
  if invisible then rand = 1250 end  --If enemy is inivisible, finish the timer after 1.25 seconds.
  
  if movement ~= nil then movement:stop() end
  state = "wandering"
  if grin then
    sprite:set_animation("walking_grin")
  else
    sprite:set_animation("walking")
  end
  movement = sol.movement.create("random_path")
  movement:set_speed(32)
  movement:start(enemy)
  sol.timer.start(enemy, rand, function() if not grin and enemy:get_distance(hero) <= detection_long and los then
                                                              enemy:grin()  
                                                            elseif invisible then
                                                              enemy:reappear()
                                                            else 
                                                              enemy:restart() 
                                                            end return false end)
end

--Use path finding to search for the hero.
function enemy:find_hero()

  state = "searching"
  movement = sol.movement.create("path_finding")
  movement:set_target(hero)
  movement:set_speed(56)
  movement:start(enemy)
  sol.timer.start(enemy, 1250, function() if inivisible then enemy:reappear() end if enemy:get_distance(hero) < 32 then enemy:attack() elseif enemy:get_distance(hero) >= detection_long then enemy:wander() else enemy:restart() end end)
end

--Event to switch to the grinning sprite.
function enemy:grin()
  if movement ~= nil then movement:stop() end
  state = "stopped"
  sprite:set_animation("grin", function() sprite:set_animation("stopped_grin") enemy:restart() end)
  grin = true
end

--Event to switch back to non_grinning sprite.
function enemy:stop_grin()
  if movement ~= nil then movement:stop() end
  state = "stopped"
  sprite:set_animation("stop_grin", function() sprite:set_animation("stopped") end)
  grin = false
end

--Event called when the enemy is supposed to attack the enemy.
function enemy:attack()
  
  state = "attacking"
  if movement ~= nil then movement:stop() end
  sprite:set_animation("attack", enemy:restart())
end

--Event to turn the enemy sprite invisible.
function enemy:invisible()
  invisible = true
  sprite:fade_out(10, function() last_anim = sprite:get_animation() sprite:set_animation("shimmer") sprite:set_opacity(255) opacity = sprite:get_opacity() sol.timer.start(enemy, 10, function() if enemy:get_distance(hero) < detection_short then enemy:find_hero() else enemy:wander() end return false end) end)
end

--Event to return the enemy sprite to opaque.
function enemy:reappear()
  sprite:set_opacity(0)
  sprite:set_animation(last_anim)
  last_anim = nil
  invisible = false 
  sprite:fade_in(10, function() if state == "searching" then enemy:attack() else enemy:restart() end end)
end

--When the hero breaches detection_long, turn to face the hero.
function enemy:face_hero()
  local hero_dist = enemy:get_distance(hero)
  local hero_dir = enemy:get_direction4_to(hero)
  local dir = sprite:get_direction()
  state = "stopped"
  if movement~= nil then movement:stop() end
  sprite:set_animation("stopped")
  sprite:set_direction(hero_dir)
  enemy:restart()
end
--Check the hero's distance and position to determine if the enemy needs to change states.
--[[
function enemy:check_hero()
  local hero_dist = enemy:get_distance(hero)
  local h_x, h_y, h_l = hero:get_position()
  local los = enemy:has_los(hero)

--If the enemy can't see the hero or the hero is too far away and the enemy hasn't 
--seen the hero yet then call enemy:stop().
  if not los and hero_dist > detection_long then
    enemy:stop()
  end

--If the enemy can see the hero and the enemy is not grinning and the hero distance
--is less than the long detection distance then make the enemy grin.
  if los and hero_dist <= detection_long and not grin then
    enemy:grin()
  end

--If the enemy can see the hero and the 
  

  sol.timer.start(enemy, 100, function() enemy:check_hero() return true end)
end
--]]

--Necessary to maintain proper sprite direction.
function enemy:on_movement_changed(movement)
  local sprite = self:get_sprite()
  local n_dir = movement:get_direction4()
  local dir = sprite:get_direction()
  if n_dir ~= dir then
      sprite:set_direction(n_dir)
  end
end

--When the enemy dies, check and execute specific code blocks.
function enemy:on_dead()
  local name = enemy:get_name()
  if enemy:get_property("split") ~= nil then
    local x, y, l = enemy:get_position()
    local split = tonumber(enemy:get_property("split"))
    for i = 1, split do
      local new_enemy = map:create_enemy{name = "hydra", layer = l, x = math.random((x - 16), (x + 16)), y = math.random((y - 16), (y + 16)), direction = 3, breed = "subesin",}
      new_enemy:set_property("split", "2")--TODO?:Add a limiter to how long the enemy can reproduce.
    end
  end
end