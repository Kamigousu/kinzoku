-- Lua script of enemy ghost_minion.
-- This script is executed every time an enemy with this model is created.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation for the full specification
-- of types, events and methods:
-- http://www.solarus-games.org/doc/latest

local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local floor = map:get_floor()
local hero = map:get_hero()
local sprite
local movement



local tier = 2
local bonus_drop
local base_exp = 50
local loot = enemy:set_treasure(enemy:get_loot(tier, floor))

-- Event called when the enemy is initialized.
function enemy:on_created()

  -- Initialize the properties of your enemy here,
  -- like the sprite, the life and the damage.
  sprite = enemy:create_sprite("enemies/" .. enemy:get_breed())
  --enemy:set_traversable(false)
  enemy:set_life(1)
  enemy:set_damage(25)
end
--[[
-- Event called when the enemy should start or restart its movements.
-- This is called for example after the enemy is created or after
-- it was hurt or immobilized.
function enemy:on_restarted()
  if self:get_distance(hero) < 200 then
    movement = sol.movement.create("path_finding")
    movement:set_target(hero)
    movement:set_speed(48)
  else
    movement = sol.movement.create("random")
    movement:set_speed(32)
  end
  movement:start(enemy)
end

function enemy:on_dying()
  math.randomseed(os.clock())
  local x, y, layer = self:get_position()
  local chance = math.random(100)
  if chance >= 95 then  --you have a 5% chance for this to execute.
    math.randomseed(os.clock() * math.pi * chance)    --for each case, multiply the clock to create a more random seed than the last.
    bonus_drop = math.random(1,3)

  elseif 75 <= chance and chance < 95 then  --you have a 2% chance for this to execute.
    math.randomseed(os.clock() * math.pi * chance)
    bonus_drop = math.random(1,2)

  elseif 25 <= chance and chance < 75 then  --you have a 50% chance for this to execute.
    math.randomseed(os.clock() * math.pi * chance)
    bonus_drop = 1
  else  --you have a 25% chance for this to execute.
    self:set_treasure(nil)
  end
return bonus_drop
end

function enemy:on_dead()
  local map = enemy:get_map()
  local bonus_drop = bonus_drop   --# of extra items dropped.
  local c_treasure, c_variant = self:get_treasure() --common treasure (currency)
  local x, y, layer = self:get_position()
  --local uc_treasure = self:get_shield() --uncommon treasure (offhand)
  --local rare_treasure = self:get_weapon() --rare treasure (main hand)

--print(bonus_drop) --print the current multiplier.
  if bonus_drop ~= nil then
    --the following for loop is the heart of this function. It lays out multiple currency units depending on a random number generator.
    for i = 1, bonus_drop do
      local clock = os.clock()
      math.randomseed(clock * (math.pi * i))    --multiply pi by the loop iteration to create a large difference in the random numbers for   each rupee/iron spawned.
      local irons_prop = {    --properties table for irons.
        ["layer"] = layer,
        ["x"] = x + (math.random(-16, 16)),   --using a random range to mix up the placement of each iron spawned.
        ["y"] = y + (math.random(-16, 16)),
        ["treasure_name"] = c_treasure,
        ["treasure_variant"] = c_variant,
      }
      map:create_pickable(irons_prop)
    end
  end

  --Get enemy exp value based on the amount of currency dropped.
  local amount        --The quantity of currency dropped.
  local value         --The value of the currency dropped.
  local values = {    --Table of values for each currency variant.
    1,
    5,
    10,
    20,
    50,
    100,
  }

  if bonus_drop ~= nil then
    amount = bonus_drop + 1   --amount of currency dropped is equal to the bonus drop plus the treasure that is normally dropped.
  else
    amount = 0                --if the treasure was set to nil because of the rng in on_dying() then amount is 0.
  end
  
  if c_variant ~= nil then
    value = values[c_variant]   --Value is equal to the index of the values table that is 
                                --equal to c_variant (the variant of this enemies treasure).
  else
    value = 0
  end

  exp = ((value * amount) + base_exp)   --Experience is calculated by multiplying the amount of currency 
                                        --the enemy drops by the value of the currency. It also adds a base 
                                        --amount of experience, to ensure the player is getting some experience with every kill.
  
  self:get_game():add_exp(exp)
end
--]]