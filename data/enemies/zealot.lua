-- Lua script of enemy zealot.
-- This script is executed every time an enemy with this model is created.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation for the full specification
-- of types, events and methods:
-- http://www.solarus-games.org/doc/latest

local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()
local sprite
local movement

local bonus_drop
local opacity
local tier = 1  --This is the 'power level' of the enemy. Higher tiers are more powerful; 0/1 is the lowest tier.
local life = 3
local damage = math.random(12, 22)  --Damage is random for each enemy.
local detection_long = 450  --Maximum distance the hero can be detected from.
local detection_short = 200  --Pathfinding movement cannot exceed a distance of 200 pixels or it will cause unpredictable problems.
--local exp --Unnecessary until experience is used.
--local base_exp = 5  --Unnecessary until experience is used.
local has_seen_hero = false --This enemy has not seen the hero yet, by default.
local state
local los
local last_anim
local loot = enemy:set_treasure(enemy:get_loot(tier, floor))  --This is the reward the enemy will drop upon death.


-- Event called when the enemy is initialized.
function enemy:on_created()

  -- Initialize the properties of your enemy here,
  -- like the sprite, the life and the damage.
  sprite = enemy:create_sprite("enemies/" .. enemy:get_breed())
  enemy:set_life(life)
  enemy:set_damage(damage)
end

-- Event called when the enemy should start or restart its movements.
-- This is called for example after the enemy is created or after
-- it was hurt or immobilized.
function enemy:on_restarted()
  local hero_dist = enemy:get_distance(hero)
  local dir = sprite:get_direction()
  local hero_dir = enemy:get_direction4_to(hero)
  los = enemy:has_los(hero)

  if hero_dist <= detection_long and dir ~= hero_dir then
    enemy:face_hero()
  end

  if hero_dist <= detection_long and dir == hero_dir and los then
    enemy:charge()
  end

  if not los or hero_dist > detection_long then
    enemy:wander()
  end
end

--Enemy wanders randomly.
function enemy:wander()

  local rand = math.random(750, 1250)
  state = "wandering"
  los = enemy:has_los(hero)

  if los then face_hero() end
  
  if movement ~= nil then movement:stop() end
  sprite:set_animation("walking")
  movement = sol.movement.create("random_path")
  movement:set_speed(32)
  movement:start(enemy)
  sol.timer.start(enemy, rand, function() enemy:restart() end)
end

--When the hero breaches detection_long, turn to face the hero.
function enemy:face_hero()
  local hero_dist = enemy:get_distance(hero)
  local hero_dir = enemy:get_direction4_to(hero)
  local dir = sprite:get_direction()
  los = enemy:has_los(hero)
  state = "stopped"

  if movement~= nil then movement:stop() end
  sprite:set_animation("stopped")
  sprite:set_direction(hero_dir)
  
  if los and hero_dist <= detection_short then
    enemy:charge()
  else
    enemy:restart()
  end
end

--Enemy charges the hero to make an attack.
function enemy:charge()
  state = "charging"
  los = enemy:has_los(hero)

  if movement ~= nil then movement:stop() end
  sprite:set_animation("walking")
  movement = sol.movement.create("path_finding")

  movement:set_speed(48)
  --movement:set_smooth(true)
  movement:start(enemy)
end


--Necessary to maintain proper sprite direction.
function enemy:on_movement_changed(movement)
  local sprite = self:get_sprite()
  local n_dir = movement:get_direction4()
  local dir = sprite:get_direction()
  if n_dir ~= dir then
      sprite:set_direction(n_dir)
  end
end