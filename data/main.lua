--Main Lua Script of the Quest.
--See the Lua API! http://www.solarus-games.org/doc/latest
--Thank you, Christopho + Co.!

require("scripts/features")
local game_manager = require("scripts/game_manager")
local console = require("scripts/console")
local quick_console = require("scripts/quick_console")
local initial_menus_config = require("scripts/menus/initial_menus_config")
local initial_menus = {}

--This function is called when Solarus starts.
function sol.main:on_started()
sol.input.set_joypad_enabled()
sol.video.set_cursor_visible(false)     ---NOTE: When set to true, this will cause the lockon/aim feature to stop working. Investigate further.
sol.video.set_fullscreen()
sol.audio.preload_sounds()

  if #initial_menus_config == 0 then
    return
  end

  for _, menu_script in ipairs(initial_menus_config) do
    initial_menus[#initial_menus + 1] = require(menu_script)
  end

  local on_top = false  -- To keep the debug menu on top.
  sol.menu.start(sol.main, initial_menus[1], on_top)
  for i, menu in ipairs(initial_menus) do
    function menu:on_finished()
      if sol.main.game ~= nil then
        -- A game is already running (probably quick start with a debug key).
        return
      end
      local next_menu = initial_menus[i + 1]
      if next_menu ~= nil then
        sol.menu.start(sol.main, next_menu)
      end
    end
  end

end

function sol.main:on_key_pressed(key, modifiers)
  local handled = false
  local time = os.time()
  
  if key == "f12" then if console.enabled == false then sol.menu.start(self, console) handled = true 
  elseif console.enabled == true then sol.menu.stop(console) end handled = true end

  if key == "f11" then sol.video.set_fullscreen(not sol.video.is_fullscreen()) handled = true end

  if key == "f4" then if quick_console.enabled == false then sol.menu.start(self, quick_console) 
  elseif quick_console.enabled == true then sol.menu.stop(quick_console) end handled = true end

  if key == "c" then
    sol.timer.start(50, function() sol.input.simulate_key_released("c") return false end)
    handled = false
  end

  return handled
end