-- Lua script of custom entity chest.
-- This script is executed every time a custom entity with this model is created.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation for the full specification
-- of types, events and methods:
-- http://www.solarus-games.org/doc/latest


---
---
---
--Custom entity designed to work like a chest but without the brandish animation.

local entity = ...
local game = entity:get_game()
local map = entity:get_map()
local hero = map:get_hero()
local sprite = sol.sprite.create("entities/chest")
local treasure = game:get_item("irons")
-- Event called when the custom entity is initialized.
function entity:on_created()
  sprite:set_animation("closed")
  -- Initialize the properties of your custom entity here,
  -- like the sprite, the size, and whether it can traverse other
  -- entities and be traversed by them.
end

function entity:on_interaction()
  sprite:set_animation("open")

end