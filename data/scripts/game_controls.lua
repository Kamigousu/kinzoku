require("scripts/multi_events")

local game_meta = sol.main.get_metatable("game")
--[[
function game_meta:on_mouse_pressed(button, x, y)
local game = self
local hero = game:get_hero()
  if button == "left" then
    if game:get_stamina() > 7 and not game:is_paused() then

      hero:start_attack()
    end
  end

end

--[[
function game_meta:on_joypad_button_pressed(button)
  local game = self
  local hero = game:get_hero()
  local stamina = game:get_stamina()
  local max_stamina = game:get_max_stamina()

  local bound_joypad = {
  attack = game:get_command_joypad_binding("attack"),
  action = game:get_command_joypad_binding("action"),
  up = game:get_command_joypad_binding("up"),
  down = game:get_command_joypad_binding("down"),
  left = game:get_command_joypad_binding("left"),
  right = game:get_command_joypad_binding("right"),
  item_1 = game:get_command_joypad_binding("item_1"),
  run = game:get_custom_joypad_command("run")
  }
print(button)
  if button == bound_joypad.run then
print(bound_joypad.run)
      sol.timer.start(400, function()
                      if self:is_hero_rolling() or self:is_hero_running() then return false end
                      if not self:is_hero_running() then
                        hero:set_walking_speed(132)
                        self:set_value("shift", true)
                        print(hero:get_walking_speed())
                      end
                      return false
                    end)
   if stamina <= 0 then
      hero:set_walking_speed(88)
      self:set_value("shift", false)
    end
  end

  if button == 1 then
    local effect = self:get_command_effect("action")
    local state = hero:get_state()
    local stamina = self:get_stamina()
    --make sure the conditions are right to dash and we're not doing something else or don't have the item that allows this
    if effect == nil and state == "free" and self:is_suspended() == false then
      local dir = hero:get_direction()
      --convert the direction we just got into radians so straight movement can use it
      if dir == 1 then dir = (math.pi/2) elseif dir == 2 then dir = math.pi elseif dir == 3 then dir = (3*math.pi/2) end
      print(hero:get_state())
      self:remove_stamina(55)
      self:set_value("rolling", true)
      local m = sol.movement.create("straight")
      m:set_angle(dir)
      m:set_speed(250)
      m:set_max_distance(75) --you may want to make this into a variable so you could upgrade the dash
      m:set_smooth(true)
      hero:freeze()
--      hero:set_blinking(true, 200) --this is just a stylistic choice. It makes it move obvious that you can dash through enemies if you enable it, but in my situation the dashing animation didn't read as clearly.
      hero:get_sprite():set_animation("rolling", function() hero:get_sprite():set_animation("rolling") end)
      --sol.audio.play_sound("swim") --this is a placeholder sound effect that everyone should have, you'd want to change it probably
      m:start(hero, function() hero:unfreeze() end)
      hero:set_invincible(true, 100) --you may want to experiment with this number, which is how long the hero is invincible for
      function m:on_obstacle_reached()
        hero:unfreeze()
      end
    end
  end
end

function game_meta:on_joypad_axis_moved(axis, state)
local game = self
local hero = game:get_hero()
local handled = false
local last_dir = hero:get_direction()
  if axis == 4 then
    if state == 1 then
      hero:set_direction(3)
      handled = true
    elseif state == 0 then
      hero:set_direction(last_dir)
      handled = true
    elseif state == -1 then
      hero:set_direction(1)
      handled = true
    end
  elseif axis == 3 then
    if state == 1 then
      hero:set_direction(0)
      handled = true
    elseif state == 0 then
      hero:set_direction(last_dir)
      handled = true
    elseif state == -1 then
      hero:set_direction(2)
      handled = true
    end
  end
return handled
end

function game_meta:on_joypad_button_released(button)
  local game = self
  local hero = self:get_hero()

  local bound_joypad = {
  attack = game:get_command_joypad_binding("attack"),
  action = game:get_command_joypad_binding("action"),
  up = game:get_command_joypad_binding("up"),
  down = game:get_command_joypad_binding("down"),
  left = game:get_command_joypad_binding("left"),
  right = game:get_command_joypad_binding("right"),
  item_1 = game:get_command_joypad_binding("item_1"),
  run = game:get_custom_joypad_command("run")
  }

  if button == bound_joypad.run then
    hero:set_walking_speed(88)
    self:set_value("shift", false)
  end
  if button == 1 then
    local effect = self:get_command_effect("action")
    local state = hero:get_state()
    local stamina = self:get_stamina()
    --make sure the conditions are right to dash and we're not doing something else or don't have the item that allows this
    if effect == nil and state == "free" and self:is_suspended() == false and not self:is_hero_running() then
      local dir = hero:get_direction()
      --convert the direction we just got into radians so straight movement can use it
      if dir == 1 then dir = (math.pi/2) elseif dir == 2 then dir = math.pi elseif dir == 3 then dir = (3*math.pi/2) end
      print(hero:get_state())
      self:remove_stamina(55)
      self:set_value("rolling", true)
      local m = sol.movement.create("straight")
      m:set_angle(dir)
      m:set_speed(325)
      m:set_max_distance(75) --you may want to make this into a variable so you could upgrade the dash
      m:set_smooth(true)
      hero:freeze()
--      hero:set_blinking(true, 200) --this is just a stylistic choice. It makes it move obvious that you can dash through enemies if you enable it, but in my situation the dashing animation didn't read as clearly.
      hero:get_sprite():set_animation("rolling", function() hero:get_sprite():set_animation("walking") end)
      sol.audio.play_sound("swim") --this is a placeholder sound effect that everyone should have, you'd want to change it probably
      m:start(hero, function() hero:unfreeze() end)
      hero:set_invincible(true, 300) --you may want to experiment with this number, which is how long the hero is invincible for
      function m:on_obstacle_reached()
        hero:unfreeze()
      end
    end
    hero:set_walking_speed(88)
    self:set_value("shift", false)
    self:set_value("rolling", false)

  end
end
--]]
return game_meta