print("Invincible!")
local game
if sol.main.get_game() ~= nil then
  game = sol.main.get_game()
end

if game ~= nil then
  local hero = game:get_hero()
  hero:set_invincible(not hero:is_invincible())
end