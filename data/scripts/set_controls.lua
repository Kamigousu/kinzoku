require("scripts/multi_events")

local function controls()
  local game = sol.main.get_game()
  local value = game:get_value("controls")
  if value == "default" then
    --Kyeboard commands.
    game:set_command_keyboard_binding("attack", nil)
    game:set_command_keyboard_binding("action", "space")
    game:set_command_keyboard_binding("up", "w")
    game:set_command_keyboard_binding("down", "s")
    game:set_command_keyboard_binding("left", "a")
    game:set_command_keyboard_binding("right", "d")
    game:set_command_keyboard_binding("pause", nil)
    game:set_command_keyboard_binding("item_1", "f")
    
    --Custom joypad commands.
    --game:set_custom_joypad_command("run", 8)
  elseif value == "classic" then
    --Keyboard commands.
    game:set_command_keyboard_binding("attack", "c")
    game:set_command_keyboard_binding("action", "space")
    game:set_command_keyboard_binding("up", "up")
    game:set_command_keyboard_binding("down", "down")
    game:set_command_keyboard_binding("left", "left")
    game:set_command_keyboard_binding("right", "right")
    game:set_command_keyboard_binding("pause", "d")
    game:set_command_keyboard_binding("item_1", "x")
    game:set_command_keyboard_binding("item_2", "z")
  
    --Joypad commands (!!!TODO!!!)
    --VVVVVVVVVVVVVVVVVVVVVVV
  
  
  
  
  end
  print("controls set...")
end

local game_meta = sol.main.get_metatable("game")
game_meta:register_event("on_started", controls)