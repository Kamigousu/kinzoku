local function stamina_managment()
  local game = sol.main.get_game()
  --This timer initiates the stamina regeneration function.
  sol.timer.start(game, 100, function() 
                              local stamina = game:get_stamina()
                              local max_stamina = game:get_max_stamina()
                              if game:is_stamina_regen() == true and not game:is_suspended() then
                                if stamina < max_stamina then
                                  game:set_stamina(stamina + (max_stamina * .0187))
                                elseif stamina >= max_stamina then
                                  game:set_stamina(max_stamina)
                                end
                              end
                            return true
                            end)

 --This timer initiates the stamina drain from running.
  sol.timer.start(game, 100, function() 
                              local stamina = game:get_stamina()
                              local max_stamina = game:get_max_stamina()
                              if game:is_hero_running() and not game:is_suspended() then
                                if stamina <= 0 then sol.input.simulate_key_released("left shift")
                                  game:set_value("shift", false)
                                end
                                  game:set_stamina(stamina - (max_stamina * .05))
                              end
                            return true
                            end)

--This timer initiates the exhaustion timer whenever the hero runs out of stamina.
  sol.timer.start(game, 100, function() 
                              local stamina = game:get_stamina()
                              local is_exhausted = game:is_hero_exhausted()
                              local action_keys = {"left_shift", "c", "space"}
                              local calls = 0

                              --local key_pressed = game:get_key_pressed()
                              if is_exhausted == true then
                                local timer = sol.timer.start(1000, function()
                                                                    if stamina < 0 then game:set_stamina(0) end 
                                                                    return false end)
                                if stamina >= 1 then timer:stop() end
                                --if key_pressed == nil then return true end
                                --if stamina < 0 then stamina = 0 end
                              end
                            return true
                            end)
end

local game_meta = sol.main.get_metatable("game")
game_meta:register_event("on_started", stamina_managment)