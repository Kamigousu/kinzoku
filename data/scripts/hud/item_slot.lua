local item_slot_builder = {}

function item_slot_builder:new(game)
  local item_slot = {}

  local hero = game:get_hero()
  local surface = sol.surface.create(80, 40)
  local item_slot_img = sol.surface.create("hud/weapon_slots.png")
  local item_displayed = sol.sprite.create("hud/items")


  function item_slot:set_dst_position(x, y)
    item_slot.dst_x = x
    item_slot.dst_y = y
  end

  local function check()
    local item_1 = game:get_item_assigned(1)

    if item_1 ~= nil then
      local name = item_1:get_name()
      if name ~= nil then
--print("item")
        item_displayed:set_animation(name)
      end
    else 
--print("no item")
        item_displayed:set_animation("empty")
    end
    return true
  end

  function item_slot:on_draw(screen)
    local x, y = item_slot.dst_x, item_slot.dst_y

   --Then the opening bracket.
    item_slot_img:draw_region(5, 5, 8, 35, screen, x + 90, y)
   --then the closing bracket.
    item_slot_img:draw_region(15, 5, 8, 35, screen, x + 118, y)

    --Draw the item sprite representing the equipped item.
    item_displayed:draw(screen, x + 108, y + 17)

  end

check()
sol.timer.start(200, check)
return item_slot






end  --end item_slot_builder:new(game)

return item_slot_builder