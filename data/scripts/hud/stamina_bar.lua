local stamina_bar_builder = {}

function stamina_bar_builder:new(game)

  local stamina_bar = {}

  local surface_draw = game:get_max_stamina() + 2
  local surface = sol.surface.create(surface_draw, 13)
  local stamina_bar_img = sol.surface.create("hud/stamina_bar.png")
  local stamina_displayed = game:get_stamina()
  local max_stamina_displayed = 0
  
  function stamina_bar:set_dst_position(x, y)
    stamina_bar.dst_x = x
    stamina_bar.dst_y = y
  end

--Checks whether the view displays the correct info
  --and updates it if necessary.
  local function check()

    local max_stamina = game:get_max_stamina()
    local stamina = game:get_stamina()

    --Maximum Stamina.
    if max_stamina ~= max_stamina_displayed then
      if stamina_displayed > max_stamina then
        stamina_displayed = max_stamina
      end
      max_stamina_displayed = max_stamina
    end

    --Current Stamina
    if stamina ~= stamina_displayed then
      local increment
      if stamina < stamina_displayed then
        increment = -1
      elseif stamina > stamina_displayed then
        increment = 1
      end
      if increment ~= 0 then
        stamina_displayed = stamina_displayed + increment

      end
    end

    return true  --Repeat the timer.
  end

  function stamina_bar:on_draw(dst_surface)
    
    local x, y = stamina_bar.dst_x, stamina_bar.dst_y
    local width, height = dst_surface:get_size()
    if x < 0 then
      x = width + x
    end
    if y < 0 then
      y = height + y
    end

    --Draw Container
    stamina_bar_img:draw_region(0, 0, surface_draw + 5, 13, dst_surface, x, y)
    --stamina_bar_img:draw_region(251, 32, 6, 13, dst_surface, x + surface_draw, y)
    local width = stamina_displayed

    --Draw content of Container
    stamina_bar_img:draw_region(2, 15, width, 7, dst_surface, x + 2, y + 3)
  end

  --Run Check()
  check()
  sol.timer.start(5, check)

  return stamina_bar
end

return stamina_bar_builder