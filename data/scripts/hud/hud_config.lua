-- Defines the elements to put in the HUD
-- and their position on the game screen.

-- You can edit this file to add, remove or move some elements of the HUD.

-- Each HUD element script must provide a method new()
-- that creates the element as a menu.
-- See for example scripts/hud/hearts.lua.

-- Negative x or y coordinates mean to measure from the right or bottom
-- of the screen, respectively.

local hud_config = {
  --Life Bar
  {
    menu_script = "scripts/hud/life_bar",
    x = 0,
    y = 1,
  },

  --Focus Bar
  --[[{
    menu_script = "scripts/hud/focus_bar",
    x = 0,
    y = 14,
  },
--]]
  --Stamina Bar
  {
    menu_script = "scripts/hud/stamina_bar",
    x = 0,
    y = 14,--y = 27
  },

  --Currency Counter
  {
    menu_script = ("scripts/hud/iron_counter"),
    x = -38,
    y = 30,
  },
  
  --Weapon Slots
  {
    menu_script = ("scripts/hud/weapon_slots"),
    x = 110,
    y = 6,
  },
  
  --Item Slot
  {
    menu_script = ("scripts/hud/item_slot"),
    x = 110,
    y = 6,
  },

  --Wave Count
  {
    menu_script = ("scripts/hud/wave_count"),
    x = 8,
    y = 27,

  },
}

return hud_config