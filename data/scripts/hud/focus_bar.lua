local focus_bar_builder = {}

function focus_bar_builder:new(game)

  local focus_bar = {}

  local surface_draw = game:get_max_focus() + 2
  local surface = sol.surface.create(surface_draw, 13)
  local focus_bar_img = sol.surface.create("hud/focus_bar.png")
  local focus_displayed = game:get_focus()
  local max_focus_displayed = 0
  
  function focus_bar:set_dst_position(x, y)
    focus_bar.dst_x = x
    focus_bar.dst_y = y
  end

--Checks whether the view displays the correct info
  --and updates it if necessary.
  local function check()

    local max_focus = game:get_max_focus()
    local focus = game:get_focus()

    --Maximum Focus.
    if max_focus ~= max_focus_displayed then
      if focus_displayed > max_focus then
        focus_displayed = max_focus
      end
      max_focus_displayed = max_focus
    end

    --Current Focus
    if focus ~= focus_displayed then
      local increment
      if focus < focus_displayed then
        increment = -1
      elseif focus > focus_displayed then
        increment = 1
      end
      if increment ~= 0 then
        focus_displayed = focus_displayed + increment

      end
    end

    return true  --Repeat the timer.
  end

  function focus_bar:on_draw(dst_surface)
    
    local x, y = focus_bar.dst_x, focus_bar.dst_y
    local width, height = dst_surface:get_size()
    if x < 0 then
      x = width + x
    end
    if y < 0 then
      y = height + y
    end

    --Draw Container
    focus_bar_img:draw_region(0, 0, surface_draw + 5, 13, dst_surface, x, y)
    --focus_bar_img:draw_region(251, 32, 6, 13, dst_surface, x + surface_draw, y)
    local width = focus_displayed

    --Draw content of Container
    focus_bar_img:draw_region(2, 15, width, 7, dst_surface, x + 2, y + 3)
  end

  --Run Check()
  check()
  sol.timer.start(game, 20, check)

  return focus_bar
end

return focus_bar_builder