local wave_counter_builder ={}

function wave_counter_builder:new(game, config)
  local wave_counter = {}
  
  local wave_text = sol.text_surface.create({font = "8_bit", horizontal_alignment = "left", vertical_alignment = "top"})
  --Set the wave text to this, "W: ". This will not change.
    wave_text:set_text("W: ")
  local count_text = sol.text_surface.create({font = "8_bit", horizontal_alignment = "left", vertical_alignment = "top"})
  --Set the count text to this, "C: ". This will not change.
    count_text:set_text("C: ")
  local wave_digits = sol.text_surface.create({font = "8_bit", horizontal_alignement = "left", vertical_alignement = "top"})
  local count_digits = sol.text_surface.create({font = "8_bit", horizontal_alignment = "left", vertical_alignment = "top"})
  local count_displayed = nil --map:get_entity_count("wave"..map:get_wave().."_")
  local dst_x, dst_y = config.x, config.y

  function wave_counter:on_draw(surface)
    
    local x, y = dst_x, dst_y
    local w, h = surface:get_size()
    if x < 0 then x = w + x end
    if y < 0 then y = h + y end
    wave_text:draw(surface, x - 8, y)
    wave_digits:draw(surface, x + 12, y + 6)
    count_text:draw(surface, x + 24, y)
    count_digits:draw(surface, x + 44, y)
  end
  
  -- Checks whether the view displays correct information
  -- and updates it if necessary.
  local function check()
    if game:get_map() ~= nil and game:get_map():get_wave() ~= nil then
      local map = game:get_map()
      local wave = map:get_wave()
      local need_rebuild = true
      local count = map:get_entities_count("wave_"..wave)
      local count_displayed = 0
      local count_clear = 0
  
      for entity in map:get_entities("wave_"..wave)  do
        if entity:is_enabled() then
          count_displayed = count_displayed + 1
        end
      end

      -- Current count.
      if count ~= count_displayed then
  
        need_rebuild = true
        if count_displayed < count then
          count_displayed = count_displayed + 1
        else
          count_displayed = count_displayed - 1
        end
  
        if count_displayed == count  -- The final value was just reached.
            or count_displayed % 3 == 0 then  -- Otherwise, play sound "iron_counter_end" every 3 values.
          --sol.audio.play_sound("iron_counter_end")
        end
      end
  
      if count_digits:get_text() == "" then
        need_rebuild = true
      end
  
      -- Update the text if something has changed.
      if need_rebuild then
        wave_digits:set_text(wave)
        count_digits:set_text(count_displayed)
  
--[[
        -- Show in green if the maximum is reached.
        if count_displayed == count_clear then
          --count_text:set_text("Floor ")
          count_digits:set_text("Clear!")
        else
          count_digits:set_font("8_bit")
        end
--]]
      end
    end
    return true  -- Repeat the timer.
  end
  
  -- Periodically check.
  check()
  sol.timer.start(40, check)
  return wave_counter
end

return wave_counter_builder