local life_bar_builder = {}

function life_bar_builder:new(game)

  local life_bar = {}

  local surface_draw = game:get_max_life() + 2
  --local surface = sol.surface.create(surface_draw, 13)
  local life_bar_img = sol.surface.create("hud/life_bar.png")
  local life_displayed = game:get_life()
  local max_life_displayed = 0
  
  function life_bar:set_dst_position(x, y)
    life_bar.dst_x = x
    life_bar.dst_y = y
  end

--Checks whether the view displays the correct info
--and updates it if necessary.
  local function check()

    local max_life = game:get_max_life()
    local life = game:get_life()

    --Maximum Life.
    if max_life ~= max_life_displayed then
      if life_displayed > max_life then
        life_displayed = max_life
      end
      max_life_displayed = max_life
    end

    --Current Life
    if life ~= life_displayed then
      local increment
      if life < life_displayed then
        increment = -1
      elseif life > life_displayed then
        if life == max_life then
          life_displayed = max_life
          increment = 0
        else
          increment = 1
        end
      end
      if increment ~= 0 then
        life_displayed = life_displayed + increment

        --Play the hurt sound at low health.
        if (life - life_displayed)%10 == 1 then
          sol.audio.play_sound("hero_hurt")
        end
      end
    end

    return true  --Repeat the timer.
  end

  function life_bar:on_draw(dst_surface)
    
    local x, y = life_bar.dst_x, life_bar.dst_y
    --[[local width, height = dst_surface:get_size()
    if x < 0 then
      x = width + x
    end
    if y < 0 then
      y = height + y
    end--]]

    --Draw Container
    life_bar_img:draw_region(0, 0, surface_draw + 5, 13, dst_surface, x, y)
    --life_bar_img:draw_region(251, 32, 6, 13, dst_surface, x + surface_draw, y)
    local width = life_displayed

    --Draw content of Container
    life_bar_img:draw_region(2, 15, width, 7, dst_surface, x + 2, y + 3)
  end

  --Run Check()
  check()
  sol.timer.start(5, check)

  return life_bar
end

return life_bar_builder