local weapon_slot_builder = {}

function weapon_slot_builder:new(game)
  local weapon_slots = {}

  local hero = game:get_hero()
  local surface = sol.surface.create(80, 40)
  local weapon_slot_img = sol.surface.create("hud/weapon_slots.png")
  local mh_weapon_displayed = sol.sprite.create("hud/main_hand")
  local oh_weapon_displayed = sol.sprite.create("hud/off_hand")

  function weapon_slots:set_dst_position(x, y)
    weapon_slots.dst_x = x
    weapon_slots.dst_y = y
  end

  local function check()
    local mh_weapon = hero:get_sword_sprite_id()
    local oh_weapon = hero:get_shield_sprite_id()

    if mh_weapon == "hero/sword" then
      mh_weapon_displayed:set_animation("sword")
    end
  end

  function weapon_slots:on_draw(screen)
    local x, y = weapon_slots.dst_x, weapon_slots.dst_y

  --Draw the mh weapon slot elements.
   --First the colored semi-circle.
    weapon_slot_img:draw_region(25, 19, 3, 5, screen, x, y - 4)
   --Then the opening bracket.
    weapon_slot_img:draw_region(5, 5, 8, 35, screen, x, y)
   --then the closing bracket.
    weapon_slot_img:draw_region(15, 5, 8, 35, screen, x + 27, y)
   -- then the title.
    weapon_slot_img:draw_region(25, 5, 32, 5, screen, x + 3, y - 6)

  --Draw the oh weapon slot elements.
   --Same order as above.
    weapon_slot_img:draw_region(30, 19, 3, 5, screen, x + 45, y - 4)
    weapon_slot_img:draw_region(5, 5, 8, 35, screen, x + 45, y)
    weapon_slot_img:draw_region(15, 5, 8, 35, screen, x + 72, y)
    weapon_slot_img:draw_region(25, 12, 32, 5, screen, x + 48, y - 6)

    --Draw the weapon sprite representing the equipped weapon.
    mh_weapon_displayed:draw(screen, x + 13, y + 8)
    oh_weapon_displayed:draw(screen, x + 58, y - 8)
  end

check()
sol.timer.start(200, check)
return weapon_slots
end

return weapon_slot_builder