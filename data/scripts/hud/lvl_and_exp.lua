-- LEVEL (LVL)and EXPERIENCE (EXP) counters shown in the game screen.

-- ZL -- LVL AND XP  version 2.0
-- from a script (for rupees) by christopho
-- Modified by froGgy for a Zelda-like (ZL) project.
-- v 1.0:  First version posted. It is just a draft.
-- Modified by Kamigousu on 18/07/19.
-- v 2.0:  Updated for use with Solarus 1.6; basic with lots of potential.
-- v 2.1:  Ongoing updates and changes c/o Kamigousu


local lvl_and_exp = {}

function lvl_and_exp:new(game)

  local object = {}
  setmetatable(object, self)
  self.__index = self

  object:initialize(game)

  return object
end

function lvl_and_exp:initialize(game)

  self.game = game
  self.surface = sol.surface.create(112, 24)
  self.digits_text_for_lvl = sol.text_surface.create{
    font = "green_digits",
    horizontal_alignment = "left",
  }
  self.digits_text_for_exp = sol.text_surface.create{
    font = "white_digits",
    horizontal_alignment = "left",
  }
  self.digits_text_for_exp_to_levelup = sol.text_surface.create{
    font = "white_digits",
    horizontal_alignment = "left",
  }
  self.digits_text_for_lvl:set_text(game:get_level())
  self.digits_text_for_exp:set_text(game:get_exp())
  self.lvl_icon_img = sol.surface.create("hud/lvl_and_exp_icon.png")
  self.exp_icon_img = sol.surface.create("hud/lvl_and_exp_icon.png")
  self.slash_icon_img = sol.surface.create("hud/lvl_and_exp_icon.png")
  self.max_level = 20
  self.current_lvl_displayed = self.game:get_level()
  self.current_exp_displayed = self.game:get_exp()
  self.current_exp_displayed_length = string.len(self.current_exp_displayed)
  self.t_exp_to_levelup = {150}
  --Fill the table with values.
  for key, value in ipairs(self.t_exp_to_levelup) do
    local next_key = key + 1
    if next_key <= self.max_level then
      self.t_exp_to_levelup[next_key] = math.ceil((self.t_exp_to_levelup[1] * next_key) + ((value * next_key) / 100))
print(#self.t_exp_to_levelup)
print(self.t_exp_to_levelup[next_key])
    else
      break
    end
  end

  self.digits_text_for_exp_to_levelup:set_text(self.t_exp_to_levelup[self.current_lvl_displayed])
  self:check()
  self:rebuild_surface()
end


function lvl_and_exp:check()

  local need_rebuild = false
  local current_level = self.game:get_level()
  local current_exp = self.game:get_exp()
  local exp_to_levelup = self.t_exp_to_levelup[current_level]
  if exp_to_levelup == nil then
	self.current_exp_displayed = self.t_exp_to_levelup[self.max_level]
	self.current_exp_displayed_length = string.len(self.current_exp_displayed)
  	exp_to_levelup = self.t_exp_to_levelup[self.max_level]
  end
  local difference = 0

	-- Current LVL.
	if current_level <= self.max_level + 1 then
	  if current_level ~= self.current_lvl_displayed then
		need_rebuild = true
		local increment
		if current_level > self.current_lvl_displayed then
		  increment = 1
		else
		  increment = -1
		end
		self.current_lvl_displayed = self.current_lvl_displayed + increment
		-- Play a sound if we have just reached the final value.
		if self.current_lvl_displayed == current_level then
		  if increment == 1 then
			sol.audio.play_sound("victory")
			sol.audio.play_sound("treasure")
		  else
			sol.audio.play_sound("switch")
			sol.audio.play_sound("hero_falls")
		  end
		end
	  end
	end

	-- Current XP.
	if current_level <= self.max_level then
	  if current_exp ~= self.current_exp_displayed then
		need_rebuild = true
		local increment
		if current_exp > self.current_exp_displayed then
		  increment = 1
		else
		  increment = -1
		end
		self.current_exp_displayed = self.current_exp_displayed + increment
		self.current_exp_displayed_length = string.len(self.current_exp_displayed)
	  end

	-- Level up
	  if self.current_exp_displayed >= exp_to_levelup then
		self.game:set_value("current_level", current_level + 1)
		difference = current_exp - exp_to_levelup
		self.game:set_value("current_exp", difference)
		current_exp = self.game:get_value("current_exp")
		self.current_exp_displayed = 0
		self.current_exp_displayed_length = string.len(self.current_exp_displayed)
	  end
	end

  -- Redraw the surface only if something has changed.
  if need_rebuild then
    self:rebuild_surface()
  end

  -- Schedule the next check.
  sol.timer.start(self.game, 40, function()
    self:check()
  end)
end

function lvl_and_exp:rebuild_surface()

  self.surface:clear()

  -- LVL (icon).
  self.lvl_icon_img:draw_region(0, 0, 12, 12, self.surface)

  -- XP (icon).
  self.exp_icon_img:draw_region(12, 0, 12, 12, self.surface, 27, 0)
  
  -- SLASH (icon).
  self.slash_icon_img:draw_region(24, 0, 8, 12, self.surface, 35 + (8 * self.current_exp_displayed_length), 4)

    -- Current LVL (counter).
  if self.current_lvl_displayed > self.max_level then
    self.digits_text_for_lvl:set_font("green_digits")
  else
    self.digits_text_for_lvl:set_font("white_digits")
  end
  self.digits_text_for_lvl:set_text(self.current_lvl_displayed)
  self.digits_text_for_lvl:draw(self.surface, 12, 6)
  
  -- Current XP (counter).
  if self.current_lvl_displayed <= self.max_level then
    self.digits_text_for_exp:set_font("white_digits")
    self.digits_text_for_exp:set_text(self.current_exp_displayed)
    self.digits_text_for_exp_to_levelup:set_font("white_digits")
	self.digits_text_for_exp_to_levelup:set_text(self.t_exp_to_levelup[self.current_lvl_displayed])
  else
    self.digits_text_for_exp:set_font("green_digits")
    self.digits_text_for_exp:set_text(self.current_exp_displayed)
    self.digits_text_for_exp_to_levelup:set_font("green_digits")
	self.digits_text_for_exp_to_levelup:set_text(self.t_exp_to_levelup[self.max_level])
  end
  self.digits_text_for_exp:draw(self.surface, 40, 6)
  self.digits_text_for_exp_to_levelup:draw(self.surface, 40 + (8 * self.current_exp_displayed_length), 12)
end

function lvl_and_exp:set_dst_position(x, y)
  self.dst_x = x
  self.dst_y = y
end

function lvl_and_exp:on_draw(dst_surface)

  local x, y = self.dst_x, self.dst_y
  local width, height = dst_surface:get_size()
  if x < 0 then
    x = width + x
  end
  if y < 0 then
    y = height + y
  end

  self.surface:draw(dst_surface, x, y)
end

return lvl_and_exp

