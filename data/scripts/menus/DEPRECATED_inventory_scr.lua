local inventory_scr = {}

local inventory_manager = require("scripts/inventory_manager")


--List of all possible weapons.
local inv_weapons = {
  "sword",
  "spear",
  "axe",
  "greatsword",
  "bow",
}

--list of all possible offhand weapons.
local inv_offhand = {
  "shield",
  "greatshield",
  "torch",
}

--List of all armours.
local inv_armour = {
  "basic",
}

--List of all items.
local inv_items = {
  "potion_health",
  "potion_stamina",
  "potion_focus",
}

local white = {255, 255, 255, 255}
local black = {0, 0, 0, 255}

local bg_pane = sol.surface.create(320, 240)
  bg_pane:fill_color(black)
local bg_draw_pane = sol.surface.create(320, 240)
local inventory_pane = sol.surface.create(192, 144)
  inventory_pane:set_xy(16, 16)
  inventory_pane:fill_color(white)
local inventory_columns = 4
local inventory_rows = 3


local description_pane = sol.surface.create(84, 144)
  description_pane:set_xy(220, 16)
  description_pane:fill_color(white)
--TODO: create text_surface's for the title and body of the description_pane.

local mainhand_tile = sol.surface.create(48, 48)
  mainhand_tile:set_xy(16, 176)
  mainhand_tile:fill_color(white)

local offhand_tile = sol.surface.create(48, 48)
  offhand_tile:set_xy(72, 176)
  offhand_tile:fill_color(white)

local armour_tile = sol.surface.create(48, 48)
  armour_tile:set_xy(128, 176)
  armour_tile:fill_color(white)

local item1_tile = sol.surface.create(48, 48)
  item1_tile:set_xy(200, 176)
  item1_tile:fill_color(white)

local item2_tile = sol.surface.create(48, 48)
  item2_tile:set_xy(256, 176)
  item2_tile:fill_color(white)

local game = sol.main.get_game()
local screen = game:get_map():get_camera():get_surface()

local draw_all

function inventory_scr:on_started()
  draw_all = false
  if game ~= nil then
    game:set_pause_allowed(false)
  end
  bg_pane:fade_in()

end

function inventory_scr:on_draw(screen)
  bg_pane:draw(screen)
  --bg_draw_pane:draw(screen)
  inventory_pane:draw(bg_pane)
  description_pane:draw(bg_pane)
  mainhand_tile:draw(bg_pane)
  offhand_tile:draw(bg_pane)
  armour_tile:draw(bg_pane)
  item1_tile:draw(bg_pane)
  item2_tile:draw(bg_pane)
end

function inventory_scr:on_key_pressed(key, modifier)
  local handled = false

end

function inventory_scr:on_finished()
  bg_pane:fade_out()
  game:set_pause_allowed()
end

return inventory_scr