--[[
This script is based off of the original solarus_logo.lua script by Maxs.
--]]
local jp_solarus_logo_menu = {}

--Create main surface of the menu.
local surface = sol.surface.create(320, 240)

local title = sol.sprite.create("menus/jp_solarus_logo/jp_solarus_logo")
title:set_animation("solarus")

local subtitle = sol.sprite.create("menus/jp_solarus_logo/jp_solarus_logo")
subtitle:set_animation("arpg")

local sun = sol.sprite.create("menus/jp_solarus_logo/jp_solarus_logo")
sun:set_animation("sun")


local black_rectangle = sol.surface.create(320, 120)
black_rectangle:fill_color{0, 0, 0}

local black_background = sol.surface.create(320, 240)
black_background:fill_color{0, 0, 0}

local anim_step = 0

local timer = nil

-------------------------------------------------------------------------------------------------------------------------------------------------------------
local function rebuild_surface()

  surface:clear()

  -- Draw the sun.
  sun:draw(surface, 55, 144)

  -- Draw the black square to partially hide the sun.
  black_rectangle:draw(surface, 55, 144)


  -- Draw the title (after step 1).
  if animation_step >= 1 then
    title:draw(surface, 55, 71)
  end

  -- Draw the subtitle (after step 2).
  if animation_step >= 2 then
    subtitle:draw(surface, 65, 124)
  end
end
-------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Starting the menu.
function jp_solarus_logo_menu:on_started()

  -- Initialize or reinitialize the animation.
  animation_step = 0
  timer = nil
  surface:set_opacity(255)
  sun:set_direction(1)
  sun:set_xy(0, 0)
  -- Start the animation.
  jp_solarus_logo_menu:start_animation()
  -- Update the surface.
  rebuild_surface()
end

-- Animation step 1.
function jp_solarus_logo_menu:step1()

  animation_step = 1
  -- Change the sun color.
  sun:set_direction(0)
  -- Stop movements and replace elements.
  sun:stop_movement()
  --sun:set_xy(55, 55)
  -- Play a sound.
  --sol.audio.play_sound("jp_solarus_logo/jp_solarus_logo")
  -- Update the surface.
  rebuild_surface()
end

-- Animation step 2.
function jp_solarus_logo_menu:step2()

  animation_step = 2
  -- Update the surface.
  rebuild_surface()
  -- Start the final timer.
  sol.timer.start(jp_solarus_logo_menu, 750, function()
    surface:fade_out()
    sol.timer.start(jp_solarus_logo_menu, 1000, function()
      sol.menu.stop(jp_solarus_logo_menu)
    end)
  end)
end

-- Run the logo animation.
function jp_solarus_logo_menu:start_animation()

  -- Move the sun.
  local sun_movement = sol.movement.create("target")
  sun_movement:set_speed(96)
  sun_movement:set_target(0, -92)
  -- Update the surface whenever the sun moves.
  function sun_movement:on_position_changed()
    rebuild_surface()
  end

  -- Start the movements.
  sun_movement:start(sun, function()

      if not sol.menu.is_started(jp_solarus_logo_menu) then
        -- The menu may have been stopped, but the movement continued.
        return
      end

      -- If the animation step is not greater than 0
      -- (if no key was pressed).
      if animation_step <= 0 then
        -- Start step 1.
        jp_solarus_logo_menu:step1()
        -- Create the timer for step 2.
        timer = sol.timer.start(jp_solarus_logo_menu, 250, function()
          -- If the animation step is not greater than 1
          -- (if no key was pressed).
          if animation_step <= 1 then
            -- Start step 2.
            jp_solarus_logo_menu:step2()
          end
        end)
      end
  end)

--Save the current sound volume to set once the sound is played.
  local db = sol.audio.get_sound_volume()
--Lower the sound volume before playing the sound.
  sol.audio.set_sound_volume(25)
  sol.audio.play_sound("jp_solarus_logo/jp_solarus_logo")
--Reset the sound volume.
  sol.audio.set_sound_volume(db)

end

-- Draws this menu on the quest screen.
function jp_solarus_logo_menu:on_draw(screen)

  -- Get the screen size.
  local width, height = screen:get_size()
  black_background:draw(screen)
  -- Center the surface in the screen.
  surface:draw(screen    ,0, 0
                     )

end

-- Called when a keyboard key is pressed.
function jp_solarus_logo_menu:on_key_pressed(key)

  if key == "escape" then
    -- Escape: quit Solarus.
    sol.main.exit()
  else
    sun:set_xy(0, -92)
    -- If the timer exists (after step 1).
    if timer ~= nil then
      -- Stop the timer.
      timer:stop()
      timer = nil
      -- If the animation step is not greater than 1
      -- (if the timer has not expired in the meantime).
      if animation_step <= 1 then
        -- Start step 2.
        jp_solarus_logo_menu:step2()
      end

    -- If the animation step is not greater than 0.
    elseif animation_step <= 0 then
      -- Start step 1.
      jp_solarus_logo_menu:step1()
      -- Start step 2.
      jp_solarus_logo_menu:step2()
    end

    -- Return true to indicate that the keyboard event was handled.
    return true
  end
end


return jp_solarus_logo_menu