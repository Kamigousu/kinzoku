local shop_menu = {}

  shop_menu.game = sol.main.get_game()
  shop_menu.hero = shop_menu.game:get_hero()
  shop_menu.npc = shop_menu.game:get_map():get_entity("shop")
  shop_menu.screen = shop_menu.game:get_map():get_camera():get_surface()
  shop_menu.main = sol.surface.create(288, 168)
  shop_menu.buy = sol.surface.create("menus/shop/buy.png")
  shop_menu.sell = sol.surface.create("menus/shop/sell.png")
  shop_menu.highlight = sol.surface.create("sprites/menus/shop/highlight.png")
  shop_menu.colour = {160, 120, 70} --Tan
  shop_menu.main:fill_color(shop_menu.colour)
  shop_menu.focus = nil

function shop_menu:get_focus()
  shop_menu.focus = shop_menu.game:get_value("shop_focus")
  if shop_menu.focus == nil then
    shop_menu.focus = "buy"
  end
return shop_menu.focus
end

function shop_menu:set_focus(focus)
  local new_focus = focus
  if new_focus == "buy" or new_focus == "sell" then
    shop_menu.focus = new_focus
    return shop_menu.focus
  else
    return shop_menu:get_focus()
  end
end

function shop_menu:on_draw()
local focus = shop_menu:get_focus()

  if shop_menu:is_started() and shop_menu.hero:get_distance(shop_menu.npc) < 64 then
    shop_menu.main:draw(shop_menu.screen, 16, 16)
    if focus == "buy" then
      shop_menu.sell:draw(shop_menu.main, 8, 8)
      shop_menu.buy:draw(shop_menu.main, 8, 8)
    elseif focus == "sell" then
      shop_menu.buy:draw(shop_menu.main, 8, 8)
      shop_menu.sell:draw(shop_menu.main, 8, 8)
    end
  else
    sol.menu.stop(self)
  end
end

function shop_menu:on_key_pressed(key, modifier)
  local handled = false
  if key == "d" then
    if focus == "buy" then
      shop_menu:set_focus("sell")
      handled = true
      return handled
    end
  end
  if key == "a" then
    if focus == "sell" then
      shop_menu:set_focus("buy")
      handled = true
      return handled
    end
  end
end

function shop_menu:is_started()
  return shop_menu.game:get_value("shop")
end

return shop_menu