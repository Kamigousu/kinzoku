
local title_screen_menu = {}

local game_manager = require("scripts/game_manager")
local quest_manager = require("scripts/quest_manager")

--main sprite (title screen card)
local title = sol.sprite.create("menus/title_screen/title_screen")



--event called when this menu is on screen
function title_screen_menu:on_started()
  title:fade_in()
  sol.audio.play_music("triforce")
end

--draw the menu on the quest screen
function title_screen_menu:on_draw(screen)

title:draw(screen, 8, 0)

end

--start the game if the player presses the "start" button
function title_screen_menu:on_joypad_button_pressed(button)
  if button == 7 then
    local game = game_manager:create("save_1.dat")
    game:start()
    quest_manager:initialise_quest()
    print("Quest initialised!")
    sol.menu.stop(self)
  end
end

--start the game if the player presses the "space bar"
function title_screen_menu:on_key_pressed(key, modifiers)

  if key == "space" then
    local game = game_manager:create("save_1.dat")
    game:start()
    quest_manager:initialise_quest()
    print("Quest initialised!")
    sol.menu.stop(self)
  end

  if key == "escape" then
    sol.main.exit()
  end

end

--event called when this menu is closing
function title_screen_menu:on_finished()
  title:fade_out()
  sol.audio.stop_music()
end


return title_screen_menu