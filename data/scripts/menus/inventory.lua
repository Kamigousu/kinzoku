local inventory = {}


--Colours
  local black = {0, 0, 0, 255}
  local trans_black = {0, 0, 0, 100}
  local dark_red = {95, 30, 30, 255}
  local trans_pink = {220, 110, 110, 255}
  local white = {255, 255, 255, 255}


--Local values
  local game = sol.main.get_game()
  local storage_pane = sol.surface.create(204, 65)
  local equipment_pane = sol.surface.create(200, 32)
  local description_pane = sol.surface.create(104, 32)
  local storage_region_y = 0
  local storage_rows = 3
  local storage_columns = 10
  local cursor_row
  local cursor_column
  local sprite = sol.sprite.create("menus/inventory/inventory_elements")
  local scroll_surface = sol.surface.create(2, 20)
  local scroll_bar = sol.surface.create(2, 5)
  local tile_x
  local tile_y
  local description  --Boolean for whether the description pane is visible.
  local need_rebuild
  local current_inventory = game:get_current_inventory()
  local expanded  --Boolean for whether or not the storage panel is expanded.

local debugging_surface


--Table properties
  inventory.bg = sol.surface.create(320, 40)
  inventory.cursor = sprite
  inventory.cursor:set_animation("cursor")
  inventory.enabled = nil
  inventory.currently_selected_tile = nil


  local function get_current_index()
    local column = cursor_column
    local row = cursor_row
    return column, row
  end

  local function set_current_index(col, row)

    if col <= 10 then
      cursor_column = col
    end
    
    if row <= 3 then
      cursor_row = row
    end

  end

  local function get_surface_center(surface)
    local w, h = surface:get_size()
    return math.floor(w/2), math.floor(h/2)
  end


--Convert grid coordinates (index_x/y) into actual screen pixels (pos_x/y). This returns the center of the tile.
  local function get_tile_at_index(index_x, index_y)
    local x, y = index_x, index_y
    local pos_x, pos_y = 12 + ((index_x - 1) * 20), 12

    return pos_x, pos_y
  end


--Convert grid coordinates (index_x/y) into actual pixels (pos_x/y). This returns the center of the tile.
--Meant to be used in reference to the storage_pane.
  local function get_current_index_tile()
    local index_x, _ = get_current_index()
    local pos_x, pos_y = 12 + ((index_x - 1) * 20), 12

    return pos_x, pos_y
  end
  
  local function get_next_index_tile()
    local index_x, index_y = get_current_index()
    local next_index_x, next_index_y = index_x + 1, index_y

    if next_index_x > 9 then
      --reset next_index_x
      next_index_x = 1
      next_index_y = next_index_y + 1
      if next_index_y > 3 then next_index_y = 1 end
    end

    local pos_x, pos_y = 12 + ((next_index_x - 1) * 20), 12

    return pos_x, pos_y
  end

  local function get_item_at_index(index_x, index_y)
    

  end

  local function get_current_inventory_grid_index(current_inventory_index)
    local index = current_inventory_index
    local grid_x, grid_y

    if index <= 10 then
      grid_y = 1
      grid_x = math.ceil(index / grid_y)
    elseif index > 10 and index <= 20 then
      grid_y = 2
      grid_x = math.ceil(index / grid_y)
    elseif index > 20 and index <= 30 then
      grid_y = 3
      grid_x = math.ceil(index / grid_y)
    end

    return grid_x, grid_y
  end


  local function build_current_storage()
    local index_x, index_y = get_current_index_tile()
    local index = index_x * index_y
    
    if current_inventory ~= nil then
      for key, item in ipairs(current_inventory) do
        item.x = index_x
        item.y = index_y
        item.index = item.x * item.y

        index_x, index_y = get_next_index_tile()
      end
    end


  end


  function inventory:get_current_item()
    local x, y = get_current_index()
    local index = x * y
    if current_inventory[index] ~= nil then
      return current_inventory[index].object
    end
  end

  function inventory:get_current_item_name()
    local x, y = get_current_index()
    local index = x * y
    if current_inventory[index] ~= nil then
      return current_inventory[index].name
    end
  end




  function inventory:on_started()
    need_rebuild = true     --initialised as true to draw the menu the first time upon opening
    inventory.enabled = true
    description = false
    expanded = false

    set_current_index(1, 1)    --Reset the cursor to the first storage position.

    inventory.bg:fill_color(black)
    storage_pane:fill_color(dark_red)

    scroll_surface:fill_color(trans_black)
    scroll_bar:fill_color(white)

  end

  function inventory:on_key_pressed(key, modifier)
    local handled = false

    if key == "up" then
      if cursor_row > 1 then
        cursor_row = cursor_row - 1
      elseif cursor_row < 1 then
        cursor_row = 1
      end
      storage_region_y = storage_region_y - 20
      if storage_region_y < 0 then storage_region_y = 0 end
      need_rebuild = true

      handled = true
    end

    if key == "down" then
      if cursor_row < 3 then
        cursor_row = cursor_row + 1
      elseif cursor_row > 3 then 
        cursor_row = 3 
      end
      storage_region_y = storage_region_y + 20
      if storage_region_y > 40 then storage_region_y = 40 end
      need_rebuild = true

      handled = true
    end

    if key == "left" then
      if cursor_column > 1 then
        set_current_index((cursor_column - 1), cursor_row)
        need_rebuild = true
      end
      handled = true
    end

    if key == "right" then
      if cursor_column < 10  then
        set_current_index((cursor_column + 1), cursor_row)
        need_rebuild = true
      end
      handled = true
    end

    if key == "tab" then
      description = not(description)
      handled = true
    end
    
    if key == "`" then
      expanded = not(expanded)
      handled = true
    end

    if key == "e" then
      if inventory:get_current_item() then

        game:set_item_assigned(1, inventory:get_current_item())

        local item = inventory:get_current_item()

        if item.is_equipped == nil or item.is_equipped == false then item.is_equipped = true end

     else
        local item = game:get_item_assigned(1)
        if item.is_equipped == true then item.is_equipped = false end
        game:set_item_assigned(1, nil)
      end
      handled = true
    end

    return handled
  end




--Useful to identify what item is selected currently.
  function inventory:draw_current_item_name(screen)

    local name_background = sol.surface.create(96, 12)
    local name_background_2 = sol.surface.create(94, 10)
    local name_surface = sol.text_surface.create{horizontal_alignment = "left", vertical_alignment = "bottom", font = "pixel", color = black, font_size = 12}
    local index_x, index_y = get_current_index()
    local index = index_x * index_y
    local name_text

    if index <= #current_inventory then
      name_text = sol.language.get_string("item."..current_inventory[index].name..".1")
    else
      name_text = ""
    end

    name_background:fill_color(dark_red)
    name_background_2:fill_color(trans_pink)
    name_background:draw(screen, 0, 0)
    name_background_2:draw(screen, 1, 1)

    name_surface:set_text(name_text)
    name_surface:draw(screen, 2, 12)

  end  





  function inventory:draw_storage(screen)

    if need_rebuild then
      storage_pane:clear()
      storage_pane:fill_color(dark_red)
      scroll_surface:clear()
      scroll_surface:fill_color(trans_black)
    end

    inventory.bg:draw(screen, 0, 0)
    inventory:draw_storage_contents()
    inventory:draw_current_item_name(screen)
--drawable:draw_region(region_x, region_y, region_width, region_height, dst_surface, [x, y])
    if not expanded then
      storage_pane:draw_region(0, storage_region_y, 204, 24, screen, 0, 12)
    else
      storage_pane:draw_region(0, storage_region_y, 204, 44, screen, 0, 12)
    end

    inventory.cursor:draw(screen, ((cursor_column - 1) * 20) + 3, 15)
    scroll_surface:draw(screen, 201, 14)
    scroll_bar:draw(scroll_surface, 0, ((cursor_row - 1) * 5))
  end





  function inventory:draw_storage_contents()

   tile_x =4
   tile_y = 4
 --Build the icon_tiles that populate the storage_pane.
    for i = 1, 30 do
  
      if tile_y >= 80 then
      --Reset the tile_x and tile_y values to their initial values.
        if tile_x >= 200 then tile_x = 4 end
        tile_y = 4 
        break 
      end
      
      local icon_tile = sol.surface.create(16, 16)
      icon_tile:fill_color(trans_pink)
      icon_tile:draw(storage_pane, tile_x, tile_y)

      if current_inventory[i] then

        local item = current_inventory[i].object
        local sprite = sol.sprite.create("entities/items")
        local item_amount_surface 

        if item.is_stackable then

          item_amount_surface = sol.text_surface.create{
            horizontal_alignment = "left", 
            vertical_alignment = "top", 
            font = "pixel", 
            --rendering_mode = "antialiasing",
            color = white, 
            font_size = 8,
          }
          current_inventory[i].amount_surface = item_amount_surface
          current_inventory[i].grid_x, current_inventory[i].grid_y = get_current_inventory_grid_index(i)
          current_inventory[i].x, current_inventory[i].y = get_tile_at_index(current_inventory[i].grid_x, current_inventory[i].grid_y)
          local amount_string = tostring(current_inventory[i].amount)
          item_amount_surface:set_text(amount_string) 
        end

        sprite:set_animation(current_inventory[i].name)
        sprite:draw(storage_pane, tile_x + 8, tile_y + 10)
        if item_amount_surface ~= nil then
          item_amount_surface:draw(storage_pane, current_inventory[i].x + 3, current_inventory[i].y - 1)
        end

      end   --end if current_inventory[i]
  
      tile_x = tile_x + 20
      if tile_x > 200 then tile_x = 4 end
  
      if i == 10 or i == 20 then tile_y = tile_y + 20 end
    
    end  --end for i = 1, 30

  end





  function inventory:update_description_pane(screen)
    local x, y = get_current_index()
    local index = x * y
    local index_tile_x, index_tile_y = get_current_index_tile()
    local description_surface = sol.text_surface.create()
    local description_text

    if index <= #current_inventory then
      description_text = current_inventory[index].description
    else
      description_text = ""
    end


    description_surface:set_font("pixolde/Pixolde")
    description_surface:set_font_size(11)
    description_surface:set_text(name_text)
    description_surface:draw(screen, index_tile_x + 2, index_tile_y + 18)
  end






  function inventory:draw_description(screen)
    
    if need_rebuild then
      description_pane:clear()
      description_pane:fill_color(trans_black)
      description_pane:set_opacity(100)
      
    end

    local x, y = get_current_index_tile()
    --local storage_pane_x, storage_pane_y = storage_pane:get_position()
    description_pane:fill_color(trans_black)
    description_pane:set_opacity(100)
    description_pane:draw(screen, x, y + 10 )
    inventory:update_description_pane(screen)
  end





  function inventory:on_draw(screen)
    inventory:draw_storage(screen)

--If description == true then draw the description pane.
    if description and inventory:get_current_item_name() ~= nil then
      inventory:draw_description(screen)
    end

    if need_rebuild then
      need_rebuild = false
    end

  end

  function inventory:on_finished()
    inventory.enabled = false
  end





return inventory