-- Sets up all non built-in gameplay features specific to this quest.

-- Usage: require("scripts/features")

-- Features can be enabled to disabled independently by commenting
-- or uncommenting lines below.

--the main script.
require("scripts/meta/sol_main")

--deprecated/methods for metatables (hero, game, etc.)(contained all in one script. Messy.)
require("scripts/meta/game")
require("scripts/meta/old_map")
require("scripts/meta/old_hero")
require("scripts/meta/enemy_hurt_by_sword")
require("scripts/meta/line_of_sight")
require("scripts/meta/loot")
require("scripts/meta/lock_on")
require("scripts/aim")    --Needs to be "scripts/meta/hero/aim"
require("scripts/draw_cursor")    --Needs to be "scripts/meta/game/draw_cursor"

--Current Practise methods for metatables (contained within a folder of the type)
require("scripts/meta/item/item")
require("scripts/meta/enemy/get_position_on_screen")
require("scripts/meta/enemy/on_removed")
require("scripts/meta/enemy/draw_target_marker")
require("scripts/meta/hero/strafing")
require("scripts/meta/hero/get_position_on_screen")
require("scripts/meta/hero/line_of_sight")
--require("scripts/meta/map/raycast_light")


--Old method definitions for metatables. Requires revision.*
require("scripts/stamina")
require("scripts/stamina_managment")
require("scripts/focus")
require("scripts/set_controls")

--Revised scripts from the base quest package.
require("scripts/menus/kinzoku_dialogue_box")
require("scripts/hud/hud")

--Console/QuickConsole for debugging and "cheat" codes.
require("scripts/console")
require("scripts/quick_console")


return true
