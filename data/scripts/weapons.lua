local weapon_list = {
torch = {dmg=10,mit=0.8,spd=165,con=300,eqw=1.5,},
training_sword = {dmg=18,mit=0.8,spd=150,con=350,eqw=2,},
sword = {dmg=22,mit=0.12,spd=150,con=440,eqw=3.5,},
spear = {dmg=25,mit=0.28,spd=175,con=490,eqw=6,},
axe = {dmg=30,mit=0.28,spd=250,con=560,eqw=5.5,},
bow = {dmg=20,mit=0.8,spd=150,con=510,eqw=2,},
dagger = {dmg=26,mit=0.4,spd=125,con=510,eqw=1,},
greatsword = {dmg=57,mit=0.23,spd=350,con=735,eqw=8.5,},
greataxe = {dmg=71,mit=0.32,spd=475,con=845,eqw=10.5,},
shield = {dmg=0,mit=0.68,spd=0,con=672,eqw=5.5,},
greatshield = {dmg=0,mit=0.75,spd=0,con=1015,eqw=9.5,},
plasma_blade = {dmg=36,mit=0.60,spd=125,con=9999,eqw=2,},
}



return weapon_list