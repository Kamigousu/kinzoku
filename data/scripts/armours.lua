local armour_list = {
starter = {mit=0.15, eqw=6, bns="EQW-10%",},
stam1 = {mit=0.17, eqw=7.5, bns="STM+",},
stam2 = {mit=0.19, eqw=9.5, bns="STM++",},
foc1 = {mit=0.12, eqw=2, bns="FCS+",},
foc2 = {mit=0.13, eqw=3.5, bns="FCS++",},
hea1 = {mit=0.22, eqw=14.5, bns="HP+",},
hea2 = {mit=0.26, eqw=22, bns="HP++",},
cloak = {mit=0.10, eqw=2.5, bns="INVIS",},
}


return armour_list