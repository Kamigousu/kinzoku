require("scripts/multi_events")


--This function will draw a new cursor icon where ever the cursor is.
--This is used because showing the actual cursor creates issues with
--  other scripts such as the aim script and the lock_on script.
local function draw_cursor(game, surface)

  local cursor_x, cursor_y = sol.input.get_mouse_position()
  local hero_scr_x, hero_scr_y, _ = game:get_hero():get_position_on_screen()
  local cursor_sprite = sol.sprite.create("entities/cursor")
  local max_distance = 64
  local delta_x = cursor_x - hero_scr_x
  local delta_y = cursor_y - hero_scr_y

  local function limit_cursor_radius(x, y, max_dist)
      if max_dist * max_dist >= (x * x) + (y * y) then
          return x, y
      end
      local angle = math.atan2(y, x)
      return math.cos(angle) * max_dist, math.sin(angle) * max_dist
  end

  if not game:get_value("locked_on") then
    local ltd_cursor_x, ltd_cursor_y = limit_cursor_radius(delta_x, delta_y, max_distance)
    cursor_sprite:draw(surface, ltd_cursor_x + hero_scr_x, ltd_cursor_y + hero_scr_y)
  end

end

local game_meta = sol.main.get_metatable("game")
game_meta:register_event("on_draw", draw_cursor)