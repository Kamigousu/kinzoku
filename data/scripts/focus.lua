local game_meta = sol.main.get_metatable("game")

  function game_meta:get_focus()
    return self:get_value("focus")
  end
  
  function game_meta:set_focus(value)
    if value > self:get_max_focus() then value = 
      self:get_max_focus()
    end
    return self:set_value("focus", value)
  end
  
  function game_meta:add_focus(value)
    focus = self:get_value("focus") + value
    if value >= 0 then
      if focus > self:get_max_focus() then focus =
      self:get_max_focus()
      end
      return self:set_value("focus", focus)
    end
  end
  
  function game_meta:remove_focus(value)
    focus = self:get_value("focus") - value
    print("focus removed: ("..value..")")
    print("Focus: ("..focus..")")
    if focus < 0 then focus = 0 end
    if value >= 0 then
      return self:set_value("focus", focus)
    end
  end
  
  function game_meta:get_max_focus()
    return self:get_value("max_focus")
  end
  
  function game_meta:set_max_focus(value)
    if value >= 45 then  --Minimum focus
      return self:set_value("max_focus", value)
    else
      return self:set_value("max_focus", 45)
    end
  end

function game_meta:get_focus_displayed()
  local focus_disp = self:get_focus() / 2
  return focus_disp
end

function game_meta:set_focus_displayed(value)
  local max_focus_disp = self:get_max_focus_displayed()
  if value < 0 then
    value = 0
  elseif value > max_focus_disp then
    value = max_focus_disp
  end
  local focus_disp = value
  return focus_disp
end

function game_meta:get_max_focus_displayed()
  local max_focus = self:get_max_focus()
  local max_focus_disp = max_focus / 2
  return max_focus_disp
end


return game_meta