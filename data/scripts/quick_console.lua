-- A Lua quick_console that can be enabled with F12 at any time during the program.

local quick_console = {
  enabled = false,
  color = { 64, 64, 64 },  -- Background color of the quick_console area.
  history = { {} },        -- An array of commands typed (the last one is the current one).
                           -- Each command as an array of UTF-8 strings
                           -- (this is necessary to be able to erase characters).
  history_capacity = 50,   -- Maximum size of the history.
  history_position = 1,    -- Current position when browsing the history.
  input_text_surface = sol.text_surface.create{
    font = "8_bit",
    font_size = 8,
  },
  output_text_surface = sol.text_surface.create{
    font = "8_bit",
    font_size = 8,
  },
}

function quick_console:on_started()  --Display the quick_console gui when pressing F12.
  self.enabled = true
  self:build_input_text()
  print("QC enabled...")
end

function quick_console:on_finished() --Close the quick_console gui when pressing F12.
  self.enabled = false
  print("QC disabled...")
end

function quick_console:get_input_text()
  return self.input_text_surface:get_text():sub(3)  -- Remove the prompt.
end

function quick_console:get_output_text()
  return self.output_text_surface:get_text()
end

function quick_console:set_output_text(text)
  self.output_text_surface:set_text(text)
end

function quick_console:build_input_text()

  local text = "> " .. table.concat(self.history[self.history_position])
  self.input_text_surface:set_text(text)
end

function quick_console:clear()

  self.history[self.history_position] = {}
  self:build_input_text()
  self:set_output_text("")
end

function quick_console:append_input_character(character)

  local characters = self.history[self.history_position]
    if (#characters + 1) < 29
    and (#characters + 1) >= 0 then
      characters[#characters + 1] = character
      self:build_input_text()
    end

end

function quick_console:remove_input_character(index)

  local characters = self.history[self.history_position]
  if index == nil then
    index = #characters
  end

  table.remove(characters, index)
  self:build_input_text()
end

local function contains(table, cmd)
  for _, value in ipairs(table) do
    if cmd == value then
      return true
    end
  end
  return false
end

function quick_console:execute_cmd()
local commands = {
"gm",
"hp",
"stm",
"fp",
"dc",
"start_hud",
"stop_hud",
--"add_level",
--"add_exp",
"cc",
}

local input = self:get_input_text()
local is_cmd = contains(commands, input)

  function quick_console:activate_command(command)
    local command = command
    if is_cmd then
      sol.main.do_file("scripts/qc/"..command)
      self:clear()
    elseif not is_cmd then
      self:set_output_text(command.." not recognised.")
    end
  end

self:activate_command(input)
end

function quick_console:on_key_pressed(key, modifiers)

  if key == "escape" then
    self:clear()
    if self:get_output_text() == "" then
      sol.menu.stop(self)
    end
  elseif key == "backspace" then
    if self:get_output_text() ~= "" then
      self:clear()
    else
      self:remove_input_character()
    end
  elseif (key == "return" or key == "kp return")
      and not modifiers.alt and not modifiers.control then
    if self:get_output_text() ~= "" then
      self:clear()
    else
      self:execute_cmd()
    end
  elseif key == "up" then
    self:history_up()
  elseif key == "down" then
    self:history_down()
  end

  -- The debugging quick_console has exclusive focus.
  return true
end

function quick_console:on_character_pressed(character)

  local handled = false
  if not character:find("%c") then  -- Don't append control characters.

    if self:get_output_text() ~= "" then
      self:clear()
    end
    self:append_input_character(character)
    handled = true
  end

  return handled
end

function quick_console:on_joypad_button_pressed(button)
  local print_button = button
  local handled = false

  self:set_output_text(print_button)

  return true
  
end

function quick_console:on_joypad_hat_moved(hat, direction8)
  local handled = false
  
  self:set_output_text("Hat: "..hat.." and Direction: "..direction8)
end

function quick_console:on_joypad_axis_moved(axis, state)
  local handled = false

  --if axis ~= 3 and axis ~= 0 then
    self:set_output_text("Axis: "..axis.."and State: "..state)
  --end
end

function quick_console:on_draw(dst_surface)

  local width, height = dst_surface:get_size()
  --print("Width: "..width.." and ".."Height: "..height..".")
  dst_surface:fill_color(self.color, 32, height - 64, width - 64, 40)
  self.input_text_surface:draw(dst_surface, 40, height - 56)
  self.output_text_surface:draw(dst_surface, 40, height - 40)
end


function quick_console.environment_index(environment, key)

  local result = nil
  if key == "print" then
    -- Redefine print to output into the quick_console.
    result = quick_console.print
  else
    local game = sol.main.game
    if game ~= nil then
      if key == "game" then
	result = game
      elseif key == "map" then
	result = game:get_map()
      else
	local entity = game:get_map():get_entity(key)
	if entity ~= nil then
	  result = entity
	end
      end
    end
  end

  if result == nil then
    result = _G[key]
  end

  return result
end

function quick_console.print(...)

  local num_args = select("#", ...)
  local text = ""
  for i = 1, num_args do
    local arg = select(i, ...)
    if type(arg) == "string" or type(arg) == "number" then
      text = text .. arg
    else
      text = text .. type(arg)
    end
    text = text .. " "
  end
  quick_console:set_output_text(text)
end

function quick_console:history_up()

  if self:get_output_text() ~= "" then
    self:clear()
  end

  if self.history_position > 1 then
    self.history_position = self.history_position - 1
    self:build_input_text()
  end
end

function quick_console:history_down()

  if self:get_output_text() ~= "" then
    self:clear()
  end

  if self.history_position < #self.history then
    self.history_position = self.history_position + 1
    self:build_input_text()
  end
end

function quick_console:history_add_command()

  if #self.history >= self.history_capacity then
    table.remove(self.history, 1)
  end

  if self.history_position < #self.history then
    -- We are browsing the history: save the selected command as the current one.
    local copy = {}
    for _, character in ipairs(self.history[self.history_position]) do
      copy[#copy + 1] = character
    end
    self.history[#self.history] = copy
  end
  -- Create a new empty command.
  self.history_position = #self.history + 1
  self.history[self.history_position] = {}
end

return quick_console