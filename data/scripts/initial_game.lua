-- This script initializes game values for a new savegame file.
-- You should modify the initialize_new_savegame() function below
-- to set values like the initial life and equipment
-- as well as the starting location.
--
-- Usage:
-- local initial_game = require("scripts/initial_game")
-- initial_game:initialize_new_savegame(game)

local initial_game = {}

-- Sets initial values to a new savegame file.
function initial_game:initialize_new_savegame(game)
math.randomseed(os.clock())
  -- You can modify this function to set the initial life and equipment
  -- and the starting location.
  game:set_starting_location("dd_test", nil)  -- Starting location.
  --game:set_controls("default")  --Set the proper control scheme

  game:set_max_life(100)
  game:set_life(game:get_max_life())
  game:set_max_stamina(100)
  game:set_stamina(game:get_max_stamina())
  game:set_max_focus(100)
  game:set_focus(game:get_max_focus())
  game:set_max_money(99999)
  game:set_value("controls", "default")
  game:set_ability("sword", 1)
  game:set_value("initial_weapon", "sword") --TODO:CHANGE TO "training_sword"
  game:set_value("initial_armour", "starter")
  game:set_value("initial_creation", true)

  --Initialise player experience system.
    --game:set_level(1)      --Initialise the player level value for new game.
  	--game:set_exp(0)       --Initialise the player experience value for new game.

end

return initial_game
