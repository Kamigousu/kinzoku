require("scripts/multi_events")


local function aim(hero)
  local game = hero:get_game()
  local strafing = require("scripts/meta/hero/strafing_state")
  local hero_dir = hero:get_direction()
  local move_dir = hero:get_movement():get_direction4()

    function hero:face_cursor()
      local map = hero:get_map()
      local pos_x, pos_y = map:get_cursor_position()
      local dir = hero:get_direction4_to(pos_x, pos_y)
      if pos_x ~= nil and pos_y ~= nil then
        hero:set_direction(dir)
      end
    end

  sol.timer.start(hero, 10, function() if not game:get_value("locked_on") then hero:face_cursor() end return true  end)
end

local hero_meta = sol.main.get_metatable("hero")
hero_meta:register_event("on_created", aim)