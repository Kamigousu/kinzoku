require("scripts/multi_events")
local game_meta = sol.main.get_metatable("game")

function game_meta:get_level()
  return self:get_value("current_level")
end

function game_meta:set_level(level)
  --local level = level
  return self:set_value("current_level", level)
end

function game_meta:add_level(level)
  local level = level
  local c_level = self:get_level()
  local level_up = c_level + level
  return self:set_value("current_level", level_up)
end

function game_meta:get_exp()
  return self:get_value("current_exp")
end

function game_meta:set_exp(exp)
  return self:set_value("current_exp", exp)
end

function game_meta:add_exp(exp)
  local exp = exp
  local c_exp = self:get_exp()
  local exp_up = c_exp + exp
  return self:set_value("current_exp", exp_up)
end

function game_meta:get_exp_to_level()
  local level = self:get_level()
  local c_exp = self:get_exp()
  local exp
  local exp_table = {150}

  for key, value in ipairs(exp_table) do
    local next_key = key + 1
    if next_key <= 30 then
      exp_table[next_key] = math.ceil((150 * next_key) + ((value * next_key) / 100))
    else
      break
    end
  end

  exp = (exp_table[level] - c_exp)
  return exp
end

--[[


local function set_camera(game, map)
print("set camera...")
local camera = map:get_camera()
camera:set_position_on_screen(0,40)
camera:set_size(320, 200)

end

game_meta:register_event("on_map_changed", set_camera)


--]]

function game_meta:get_current_inventory()
  if game_meta.current_inventory == nil then game_meta.current_inventory = {} end
  return game_meta.current_inventory
end


function game_meta:add_to_current_inventory(item)
    local current_inventory = game_meta:get_current_inventory()

    if item.is_stackable then
--print("true")
      local found = false
      local new_stack = false
      local check_next_key = false
      if #current_inventory > 0 then

        for key, value in ipairs(current_inventory) do
--print("keyvaluepairs")
          check_next_key = false
          if value.name == item:get_name() then --print("SKJFHSKVRJHLWNHJKR")

            if value.amount == value.max_amount then --print("amount maxed, checking for next stack")


              if current_inventory[key+1] == nil then
--print("next key is nil")
                new_stack = true 

              else
                check_next_key = true
              end

            end
 
            if not new_stack or not check_next_key then
              value.amount = value.amount + 1 
              if value.amount > value.max_amount then 
                value.amount = value.max_amount
                found = false
                --print("value maxed, checking next stack")
              else
    
                found = true

              end

            end --if not new stack

          end --end if value.name == item:get_name()

        end --end for loop

      end --end if #current_inventory > 0

      if found == false and not check_next_key then
--print("found = false, creating new stack")
        current_inventory[#current_inventory + 1] = {name = item:get_name(), amount = 1, max_amount = item.max_amount, object = item,}
        if new_stack then new_stack = false end

      end --if found == false

    else  --if item is not stackable.

      current_inventory[#current_inventory + 1] = {name = item:get_name(), amount = 1, max_amount = item.max_amount, object = item,}

    end
    game_meta.current_inventory = current_inventory
end






function game_meta:remove_from_current_inventory(item)

  local current_inventory = game_meta:get_current_inventory()

--if item is stackable then remove one from the first stack found; ignore everything else.
  if item.is_stackable then
    
    for key, value in ipairs(current_inventory) do
      
      if value.name == item:get_name() then

        value.amount = value.amount - 1
        if value.amount < 0 then value.amount = 0 end
        if value.amount == 0 then 
          table.remove(current_inventory, key) 
          self:set_item_assigned(1, nil)
        end  --remove the value from the table
        break

      end --end if value.name == item:get_name()

    end --end for k,v in ipairs()


  else  --Item is not stackable and therefore should be immediately removed from the inventory.
    
  end --end if item.is_stackable

end








function game_meta:on_mouse_pressed(button, x, y)
local game = self
local hero = game:get_hero()
local handled = false
  if button == "left" then
    if game:get_stamina() > 7 and not game:is_paused() and hero:is_ready() then

      hero:start_attack()
      handled = true
    else
      handled = false
    end
  end
return handled
end


function game_meta:on_key_pressed(key, modifiers)
local handled = false
local game = self

  if key == "i" then
    local inventory = require("scripts/menus/inventory")
    if sol.menu.is_started(inventory) then
      sol.menu.stop(inventory)
    else
      sol.menu.start(self, inventory)
    end
    handled = true
  end

return handled
end


function game_meta:on_game_over_started()
  local hero = self:get_hero()
  local map = hero:get_map()
  local h_x, h_y, h_l = hero:get_position()
  local sprite = hero:get_sprite()
  local bones = {
    name = "grave",
    direction = 0,
    layer = h_l,
    x = h_x,
    y = h_y,
    width = 32,
    height = 32,
    sprite = "entities/bonepile",
  }
    --self:set_value("locked_on", false)
    sol.audio.play_sound("hero_dying")
    hero:freeze()
    sprite:set_animation("dying", function() self:start()  end)
    sprite:set_ignore_suspend()
    map:create_custom_entity(bones)
    if map:is_floor_clear() == false then
print("reset_waves")
      map:set_wave(0)
    end
    if self:get_value("death_counter") == nil then self:set_value("death_counter", 0) end
    self:set_value("death_counter", self:get_value("death_counter") + 1)

end

function game_meta:on_game_over_finished()
  local hero = self:get_hero()
  local map = hero:get_map()
  local sprite = hero:get_sprite()
    hero:unfreeze()
    sprite:set_ignore_suspend(false)  --!!!ATTN!!! : ALWAYS REMEMBER TO SET THIS TO FALSE AFTER THE GAME OVER IS COMPLETE!
end







function game_meta:get_custom_joypad_command(command)
  return self:get_value(command)
end

function game_meta:set_custom_joypad_command(command, input)
  return self:set_value(command, input)
end

--Events of game.lua
function game_meta:on_paused()
  self:start_dialog("paused.exit",function(answer) 
    if answer == 2 then   --Answer is yes.
      self:save()
      sol.main.exit()
    else
      self:set_paused(false)
    end
    sol.audio.play_sound("danger")
  end)
end

local function on_started(game)
    game:set_value("locked_on", false)
end

game_meta:register_event("on_started", on_started)