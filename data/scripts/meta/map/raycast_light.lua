require("scripts/multi_events")


--Scan all corners and return a table of the corners and their associated data.
local function scan_corners(map)
  local corners = {}    --table to hold all recorded corners
  local hero = map:get_hero()
  local corner_x, corner_y, corner_z
  local corner_distance, corner_angle
  local delta_x, delta_y

  if not map:has_entities("corner") then 
    print("no corner entities")
    return

  else

    for entity in map:get_entities("corner_") do
      corner_angle, corner_distance = hero:get_angle(entity), hero:get_distance(entity)
      corner_x, corner_y, corner_z = entity:get_position()
      for i = 0, corner_distance do
        delta_x, delta_y = math.floor(math.cos(corner_angle))*i, -math.floor(math.sin(corner_angle)*i)
        if hero:test_obstacles(delta_x, delta_y) then
          return
        else
          corners[#corners + 1] = {distance = corner_distance, angle = corner_angle, x = corner_x, y = corner_y, z = corner_z}
        end
      end
    end
  end

  return corners
end


local function trace_light(map)
  local corners = scan_corners(map)

  if corners == nil then print("no corners") return end
  
  for k, v in pairs(corners) do
    print("key: "..k)
    print("Distance: ")
    print("Angle: ")
    print("X: ")
    print("Y: ")
    print("Z: ")
    break
  end

end
local map_meta = sol.main.get_metatable("map")
map_meta:register_event("on_draw", trace_light)









































--[[

--Scan all corners and return a table of the corners and their associated data.
local function scan_corners(map)
  local corners = {}    --table to hold all recorded corners
  local hero = map:get_hero()
  local corner_x, corner_y, corner_z
  local corner_distance, corner_angle

  for entity in map:get_entities("corner") do
    corner_angle, corner_distance = hero:get_angle(entity), hero:get_distance(entity)
    corner_x, corner_y, corner_z = entity:get_position()
    corners[entity:get_name()] = {distance = corner_distance, angle = corner_angle, x = corner_x, y = corner_y, z = corner_z}
  end
print(unpack(corners))
  return corners
end



--Order corners and pair them to create walls and return those walls in a table.
local function create_walls(map)
  local walls = {}    --Table to hold the defined walls and their endpoints.
  local corners = scan_corners(map)

  return walls
end



--]]