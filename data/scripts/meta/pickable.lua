require("scripts/multi_events")

local pick_meta = sol.main.get_metatable("pickable")

local function on_removed()
  local map = self:get_map()
  local hero = map:get_hero()
  print("removed")
end
pick_meta:register_event("on_removed", on_removed)