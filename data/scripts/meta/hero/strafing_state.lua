local strafing = sol.state.create("Walking state with no control over sprite direction.")

--Set up parameters of the state.
strafing:set_can_control_direction(false)



return strafing