local hero_meta = sol.main.get_metatable("hero")

function hero_meta:has_los(entity) 

  local los = true
  local dx, dy = 0, 0
  local distance = self:get_distance(entity)
  local angle = self:get_angle(entity)
  local facing_x, facing_y, _ = self:get_facing_position()
  local facing_dir = self:get_direction4_to(facing_x, facing_y)
  local target_dir = self:get_direction4_to(entity)
  for i=0, distance do
    dx, dy = math.floor(math.cos(angle)*i), -math.floor(math.sin(angle)*i)
    if facing_dir ~= target_dir then
      los = false
      break
    else
      if self:test_obstacles(dx, dy) then
          los = false
          break
      end
    end
  end

  return los
end