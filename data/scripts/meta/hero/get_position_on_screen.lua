local hero_meta = sol.main.get_metatable("hero")

function hero_meta:get_position_on_screen()
  local hero = self
  local hero_x, hero_y, hero_z = hero:get_position()
  local camera = hero:get_map():get_camera()
  local screen = camera:get_surface()
  local cam_x, cam_y = camera:get_position()
  local cam_screen_x, cam_screen_y = camera:get_position_on_screen()
  local pos_x = hero_x - cam_x + cam_screen_x
  local pos_y = hero_y - cam_y + cam_screen_y
  return pos_x, pos_y
end