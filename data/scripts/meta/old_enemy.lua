local enemy_meta = sol.main.get_metatable("enemy")
require("scripts/multi_events")













function enemy_meta:get_loot(tier, floor)
  local loot
  local treasure
  local variant
  local roll
  local tier = tier
  local floor = floor
  local t_loot = {"a128", "b131", "c134", "d137", "e140", "f143", "g146",
                  "h149", "i152", "j155", "k158", "l161", "m164", "n167", "o170",
                  "p173", "q176", "r179", "s182", "t185",}
  if floor == nil then
    floor = 1
  end
  if tier == nil then
    tier = 1
  end
  if floor > 6 then
    floor = 6
  end

  if tier > 5 then
    tier = 5
  end

  if floor ~= 6 and tier > 4 then
    tier = 4
  end

  if floor == 1 then
    t_loot = sol.main:range(t_loot, 1, 3)
  end
  if floor == 2 then
    t_loot = sol.main:range(t_loot, 4, 7)
  end
  if floor == 3 then
    t_loot = sol.main:range(t_loot, 8, 11)
  end
  if floor == 4 then
    t_loot = sol.main:range(t_loot, 12, 15)
  end
  if floor == 5 then
    t_loot = sol.main:range(t_loot, 16, 19)
  end
  if floor == 6 then
    t_loot = sol.main:range(t_loot, 20, 20)
  end

  if tier == 4 and floor == 1 then
    loot = t_loot[tier - 1]
  elseif tier == 5 and floor == 6 then
    loot = t_loot[1]
  else
    loot = t_loot[tier]
  end

  if (tier < 5 and floor == 6) or (tier == 3 and floor == 1) then
    loot = nil
  end

  if loot == "a128" then
     treasure = 'irons'
     variant = 1
  end
  if loot == "b131" or loot == "d137" then
     roll = math.random(100)
    if roll > 80 then
       treasure = 'irons'
       variant = 1
    else
       treasure = 'irons'
       variant = 2
    end
  end
  if loot == "c134" or loot == "j155" or loot == "n167" then
     treasure = 'irons'
     variant = 3
  end
  if loot == "e140" or loot == "h149" then
     roll = math.random(100)
    if roll > 80 then
       treasure = 'irons'
       variant = 2
    else
       treasure = 'irons'
       variant = 1
    end
  end
  if loot == "f143" or loot == "q176" then
     roll = math.random(100)
    if roll > 80 then
       treasure = 'irons'
       variant = 2
    else
       treasure = 'irons'
       variant = 3
    end
  end
  if loot == "g146" or loot == "r179" then
     roll = math.random(100)
    if roll > 80 then
       treasure = 'irons'
       variant = 3
    else
       treasure = 'irons'
       variant = 4
    end
  end
  if loot == "i152" or loot == "l161" or loot == "m164" or loot == "p173" then
     treasure = 'irons'
     variant = 2
  end
  if loot == "o170" or loot == "s182" then
     treasure = 'irons'
     variant = 4
  end
  if loot == "t185" then
     roll = math.random(100)
    if roll > 95 then
       treasure = 'irons'
       variant = 6
    else
       treasure = 'irons'
       variant = 5
    end
  end

  return treasure, variant
end

--Define the Line of Sight of the enemy to the hero. default return is true.
function enemy_meta:has_los(entity) 

  local los = true
  local dx, dy = 0, 0
  local distance = self:get_distance(entity)
  local angle = self:get_angle(entity)
  local facing_pos = self:get_facing_position()
  local facing_dir = self:get_direction4_to(facing_pos)
  local hero_dir = self:get_direction4_to(self:get_map():get_hero())
  for i=0, distance do
    dx, dy = math.floor(math.cos(angle)*i), -math.floor(math.sin(angle)*i)
    if self:test_obstacles(dx, dy) then
      if facing_dir ~= hero_dir then
        los = false
        break
      end
    end
  end

  return los
end

function enemy_meta:on_hurt_by_sword(hero, enemy_sprite)
  local enemy = self
print(enemy_sprite)
  local enemy_list = require("scripts/enemies")
  local enemy_breed = enemy:get_breed()
  local enemy_type = enemy_list.enemy_breed
  local enemy_mit = enemy_type[mit]
  local hero_dmg = hero:get_property("attack_damage")
  local mit_damage = hero_dmg * (1 - enemy_mit)
  enemy:remove_life(mit_damage)
print(enemy:get_life() - mit_damage)
end

--[[
local function on_hurt_by_sword(enemy, hero, enemy_sprite)
print(enemy_sprite)
  local enemy_list = require("scripts/enemies")
  local enemy_breed = enemy:get_breed()
  local enemy_type = enemy_list.enemy_breed
  local enemy_mit = enemy_type[mit]
  local hero_dmg = hero:get_property("attack_damage")
  local mit_damage = hero_dmg * (1 - enemy_mit)
  enemy:remove_life(mit_damage)
print(enemy:get_life() - mit_damage)
end

enemy_meta:register_event("on_hurt_by_sword", on_hurt_by_sword)
--]]


--[[
function enemy_meta:on_obstacle_reached(movement)
  local hero = self:get_map():get_hero()
  local hero_dir = self:get_direction4_to(hero)
  local hero_dist = self:get_distance(hero)
  local x, y, l = self:get_position()
  local ox, oy, ol = self:get_facing_position()  --the position of the obstacle.
  local dx = ox - x
  local dy = oy - y
  local move = 16  --the distance to move the enemy each time it encounters an obstacle.
  local dir  --the clear direction closest to the hero.
  local west      --variable to store the west directions coordinates
  local east     --variable to store the east directions coordinates
  local north    --var to store the north direction's coordinates
  local south    --var to store the south direction's coordinates
  local m         --var to store movement entity.
  local dir_4     --var to store the direction of the path movement.


  --Test sides for obstacles and check distance to hero from each side.

  if dx == 0 then --enemy is facing N or S
    east = x + 16
    west = x - 16
    
    --determine the direction closest to the hero
    if (hero_dist - west) > (hero_dist - east) then
      dir = east
      dir_4 = 0   --direction4 value for the set_path{} below.
    elseif (hero_dist - west) < (hero_dist - east) then
      dir = west
      dir_4 = 4   --direction4 value for the set_path{} below.
    end

    if not self:test_obstacles(dir, y) then
      m = sol.movement.create("path")
      m:set_path{dir_4, dir_4}
      m:set_speed(32)
      m:set_loop(false)
      m:start(self)
    end

    --If west and east are blocked then try to turn back.
    if self:test_obstacles(east, y)  --if there are obstacle to the east
    and self:test_obstacles(west, y)  --and the west, then test behind and move back.
    then if not self:test_obstacles(x, y - 16) then end
    end
    
  elseif dy == 0 then --enemy is facing E or W
    north = y - 16
    south = y + 16
    
    --determine the direction closest to the hero
    if (hero_dist - south) > (hero_dist - north) then
      dir = north
    else
      dir = south
    end

    --If south and north are blocked then try to turn back.
    if self:test_obstacles(x, north)  --if there are obstacle to the north
    and self:test_obstacles(x, south)  --and the south, then test behind and move back.
    then if not self:test_obstacles(x + 16, y) then  end
    end

  end
--print("x: "..x.." y: "..y.." l: "..l..".")
--print("ox: "..ox.." oy: "..oy.." ol: "..ol..".")

end
--]]

