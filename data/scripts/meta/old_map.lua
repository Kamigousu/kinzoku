require("scripts/multi_events")

local map_meta = sol.main.get_metatable("map")
local timer
local graveyard = {}

function map_meta:get_wave()
  local game = self:get_game()

  return game:get_value("wave")
end

function map_meta:set_wave(wave)
  local game = self:get_game()

  return game:set_value("wave", wave)
end

function map_meta:get_next_wave()
  local game = self:get_game()

  local current_wave = self:get_wave()
  local next_wave = current_wave + 1
  if next_wave > 3 then next_wave = -1 end
  return next_wave
end


function map_meta:start_wave(wave)
  local wave = wave
  self:set_wave(wave)

  for entity in self:get_entities("torch_room") do
    local sprite = entity:get_sprite()
    local animation = sprite:get_animation()
      if animation == "lit" then
        sprite:set_animation("unlit")
      end
  end

  for entity in self:get_entities("wave_"..wave.."_") do
    entity:set_enabled()
  end
  sol.timer.start(self, 100, function() self:check_wave() return true end)
print("wave "..wave.."...")   --prints the current wave to the console. TODO: replace with text on screen that displays current wave.
end

function map_meta:check_wave()
--print("check")      --Enable only for debugging.
  local check = {
    cleared,
    wave = self:get_wave(),
    count = self:get_entities_count("wave_"..self:get_wave().."_"),
  }
  if check.wave == nil then check.wave = 0 end
  if check.wave == 0 then return true end --The first wave has not spawned yet, continue calling timer.
  if check.count > 0 then cleared = false else cleared = true end

  if 0 < check.wave and check.wave <= 3 then
    if self:has_entities("wave_"..check.wave.."_") == true then
      check.cleared = false
    elseif self:has_entities("wave_"..check.wave.."_") == false then
      check.cleared = true
    end
  end
count = self:get_entities_count("wave_"..check.wave.."_")
--print(count)   --displays the enemy count in the console. TODO: add enemy counter to hud (maybe?)
  if check.cleared then
    if 0 < check.wave and check.wave < 3 then
      sol.timer.stop_all(self)
      sol.timer.start(self, 15000, function() self:start_wave(check.wave + 1) return false end)
      return false
    elseif check.wave == 3 then
      --FINISHED///TODO: add instructions for finishing a floor( unlock entrance, spawn keys/chest, etc. )
      self:set_wave(-1)
      self:set_floor_clear(check.cleared)
      --if floor == 1 then
        sol.timer.start(self, 1250, function()  --self:get_entity("to_outside"):set_enabled(false)
                                                --self:get_entity("outside_light"):set_enabled()
                                                --self:get_entity("outside_light_long"):set_enabled()\
                                                for entity in self:get_entities("torch_room") do
                                                  local sprite = entity:get_sprite()
                                                  local animation = sprite:get_animation()
                                                  if animation == "unlit" then
                                                    sprite:set_animation("lit")
                                                  end
                                                end
                                                self:get_entity("chest_reward_"..self:get_floor()):set_enabled()
                                                local to_next_floor = self:get_entity("to_next_floor")
                                                to_next_floor:open() 
                                                sol.timer.stop_all(self)return false end)
      --[[else
        sol.timer.start(self, 1250, function()  self:get_entity("to_last_floor"):set_enabled(false) return false end)
      end--]]
print("waves complete...")
    end

  else
    return true
  end

end

function map_meta:check_next_wave()
  local is_next_wave = false
  local next_wave = self:get_next_wave()
  
  if next_wave == -1 then  --This is the third and final wave of this floor.
    
  end
  if self:has_entities("wave_"..next_wave.."_")then
    sol.timer.start(self, 15000, function() self:start_wave(next_wave) return false end)
  end
    
end

function map_meta:set_floor_clear(clear)    --True for clear, false for not clear.
  local floor = self:get_floor()
  return self:get_game():set_value("floor_"..floor.."_clear", clear)
end

function map_meta:is_floor_clear()
  local clear = false
  local floor = self:get_floor()
  local wave = self:get_wave()
  if wave == -1 then
    clear = true
  end
  return clear
end

function map_meta:start_overlay(overlay)    --overlay is a string used to find the overlay sprite file.
  local hero_light = sol.sprite.create("entities/"..overlay)
  local overlay = sol.sprite.create("entities/overlay")
  local surface = self:get_camera():get_surface()
  local shadow = sol.surface.create()
  local light_map = sol.surface.create()
  local h_light_map = sol.surface.create()
  local hero_dir = self:get_hero():get_direction() 
  local hx, hy, _ = self:get_hero():get_position()
  local cx, cy, _ = self:get_camera():get_position()
  local dx = hx - cx
  local dy = hy - cy
 
  shadow:set_blend_mode("multiply")
  light_map:set_blend_mode("add")
  h_light_map:set_blend_mode("add")
  
  local cosx, cosy, _ = self:get_camera():get_position_on_screen()
  overlay:draw(shadow, cosx, cosy - 40)

  hero_light:set_direction(hero_dir)
  hero_light:draw(h_light_map, dx, dy)               --create the light at the players position

  for entity in self:get_entities("torch_doorway") do
    local sprite = sol.sprite.create("entities/torch_light")       ------!ATTN!: As of 7/8/19, there will be no lit torches in the game.
    local tx, ty, _ = entity:get_position()                        ------!!!!!!  The tower is abandoned and the darkness adds to the thrill
    local d_x = tx - cx                                            ------!!!!!!  and fun of the game; as well as the exploration.
    local d_y = ty - cy                                            ------AS OF 1/27/20, Torches will be implemented and used regularly. 
                                                                   ------(Finally figured out how to get surfaces to behave properly for me.)
    sprite:draw(light_map, d_x, d_y)
  end

  local torches = {}
  for entity in self:get_entities("torch_room") do
    local sprite = sol.sprite.create("entities/torch_light")
    if entity:get_sprite():get_animation() == "lit" then
      local tx, ty, _ = entity:get_position()
      local d_x = tx - cx
      local d_y = ty - cy

      sprite:draw(light_map, d_x, d_y)
    else
      break
    end
  end

  h_light_map:draw(shadow)
  light_map:draw(shadow)
  shadow:draw(surface)

end



function map_meta:get_cursor_position()
  local map = self
  local camera = map:get_camera()
  local mouse_x, mouse_y = sol.input.get_mouse_position()
  local cam_screen_x, cam_screen_y = camera:get_position_on_screen()
  local cam_x, cam_y, _ = camera:get_position()
  local pos_x = mouse_x - cam_screen_x + cam_x
  local pos_y = mouse_y - cam_screen_y + cam_y
  return pos_x, pos_y
end



--TODO: Change all functions below into multi_events functions.
--E.G., use map_meta:register_event("on_started", function) instead of hardcoded functions like map_meta:on_started()

function map_meta:on_started()
  local map = self
  local game = map:get_game()
  print("set camera...")
  local camera = map:get_camera()
  camera:set_position_on_screen(0,40)
  camera:set_size(320, 200)

  if #graveyard > 0 then
    for _, grave in ipairs(graveyard) do
      map:create_custom_entity{grave}
print("grave_created")
    end
  end
end


function map_meta:on_finished()
  local map = self
  local game = map:get_game()
  
  for entity in map:get_entities("grave_") do
print(entity)
    local name = entity:get_name()
    local dir = entity:get_sprite():get_direction()
    local x, y, l = entity:get_position()
    local w, h = entity:get_size()
    local sprite = entity:get_sprite()
    
    local grave = {name, dir, l, x, y, w, h, sprite}
    graveyard[#graveyard + 1] = grave
print("grave_added")
  end
end

--[[
local function set_camera()
print("set camera...")
local camera = sol.main.get_game():get_map():get_camera()
camera:set_position_on_screen(0,40)
camera:set_size(320, 200)

end

map_meta:register_event("on_opening_transition_finished", set_camera)
--]]