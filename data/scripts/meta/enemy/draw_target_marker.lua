require("scripts/multi_events")



local function draw_target_marker(game, surface)
  local hero = game:get_hero()

  if game:get_value("locked_on") and game:get_value("closest_enemy") ~= nil then
    local target = game:get_map():get_entity(game:get_value("closest_enemy"))
      if target ~= nil then
        local marker = sol.sprite.create("entities/target_marker")
        local target_screen_x, target_screen_y = target:get_position_on_screen()
        marker:draw(surface, target_screen_x, target_screen_y)
      else
        return
      end
  end
end

local game_meta = sol.main.get_metatable("game")
game_meta:register_event("on_draw", draw_target_marker)