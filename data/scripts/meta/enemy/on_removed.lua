require("scripts/multi_events")



local function on_removed(enemy)
  local game = enemy:get_game()
  if enemy:get_name() == game:get_value("closest_enemy") then
    game:set_value("locked_on", false)
  end
end
local enemy_meta = sol.main.get_metatable("enemy")
enemy_meta:register_event("on_removed", on_removed)