local enemy_meta = sol.main.get_metatable("enemy")

function enemy_meta:get_position_on_screen()
  local enemy = self
  local enemy_x, enemy_y, enemy_z = enemy:get_position()
  local camera = enemy:get_map():get_camera()
  local screen = camera:get_surface()
  local cam_x, cam_y = camera:get_position()
  local cam_screen_x, cam_screen_y = camera:get_position_on_screen()
  local pos_x = enemy_x - cam_x + cam_screen_x
  local pos_y = enemy_y - cam_y + cam_screen_y
  return pos_x, pos_y
end