local enemy_meta = sol.main.get_metatable("enemy")
require("scripts/multi_events")


local function on_hurt_by_sword(enemy, hero, enemy_sprite)
print("Before: "..enemy:get_life())
  local game = hero:get_game()
  local enemy_list = require("scripts/enemies")
  local enemy_breed = enemy:get_breed()
  local enemy_type = enemy_list[enemy_breed]
  local enemy_mit = enemy_type.mit
  local hero_dmg = game:get_value("attack_damage")
  local mit_damage = hero_dmg * (1 - enemy_mit)
  enemy:remove_life(mit_damage)
print("After: "..enemy:get_life())
end

enemy_meta:register_event("on_hurt_by_sword", on_hurt_by_sword)