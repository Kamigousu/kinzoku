require("scripts/multi_events")

--Devise a function to use the 'Q' key to lock on to enemies in front of the hero and within range.
--'Q' key should also be used to remove lock on and reset the camera.



local function lock_on(game, key, modifiers)

  local handled


  if key == "q" then
    local map = game:get_map()
    local hero = map:get_hero()
    local target_marker = sol.sprite.create("entities/target_marker")
    local locked_on = game:get_value("locked_on")
    local locked_on_timer
    local line_of_sight_timer
    local range = 128  --This is the max distance, in pixels, that an enemy may be detected and locked on to.
    handled = false

    if locked_on == nil or not locked_on then
      game:set_value("locked_on", true)
      locked_on = game:get_value("locked_on")
--print(locked_on)
    elseif locked_on then
      game:set_value("locked_on", false)
      locked_on = game:get_value("locked_on")
      if locked_on_timer then
        locked_on_timer:stop()
      end
      if line_of_sight_timer then
        line_of_sight_timer:stop()
      end
--print(locked_on)
    end

    if locked_on then
--print("locking on")
      --find enemy to lock on to
      local enemy_table = {}
      local hero_facing_dir = hero:get_direction()
      local hero_pos_x, hero_pos_y, hero_pos_z = hero:get_position()

      for entity in map:get_entities() do
        if entity == nil then return end
        local type = entity:get_type()
        local entity_name = entity:get_name()
        if type == "enemy" and entity:is_enabled() then
          local pos_x, pos_y, pos_z = entity:get_position()
          local dir_from_hero = hero:get_direction4_to(entity)
          local los = hero:has_los(entity)
        --Check if the hero is looking in the direction of the enemy 
        --and that the two are on the same layer.
          if pos_z == hero_pos_z 
          and los
          then
        --Add enemy to table of enemies if hero is facing entity , the two are on the same layer, and the hero has line of sight on the entity.
            enemy_table[entity_name] = {["pos_x"] = pos_x, ["pos_y"] = pos_y, ["pos_z"] = pos_z, ["dir_from_hero"] = dir_from_hero,}
          end
        end
      end

      local closest_enemy
      local closest_enemy_x, closest_enemy_y
      local closest_enemy_delta_x, closest_enemy_delta_y

    --Get the closest enemy based on its position relative to the hero's position.
        for key, value in pairs(enemy_table) do
--print("start enemy table for loop")
          if value == nil then return end
          local pos_x, pos_y= value.pos_x, value.pos_y
        
          local delta_x = math.abs(hero_pos_x - pos_x)
          local delta_y = math.abs(hero_pos_y - pos_y)
        
          if hero_facing_dir == 0 or hero_facing_dir == 2 then
--print("0/2")
            if closest_enemy_delta_x == nil or closest_enemy_delta_x > delta_x then
              closest_enemy_delta_x = delta_x
              closest_enemy_x = pos_x
              closest_enemy = map:get_entity(key)
            end
          end

          if hero_facing_dir == 1 or hero_facing_dir == 3 then
--print("1/3")
            if closest_enemy_delta_y == nil or closest_enemy_delta_y > delta_y then
              closest_enemy_delta_y = delta_y
              closest_enemy_y = pos_y
              closest_enemy = map:get_entity(key)
            end
          end
        --end the for pairs loop.
        end
        
        if closest_enemy ~= nil then
--print("printing closest enemy...\n"..closest_enemy:get_name())
          game:set_value("closest_enemy", closest_enemy:get_name())
        else
--print("closest enemy not found")
          game:set_value("closest_enemy", nil)
          game:set_value("locked_on", false)
          locked_on = game:get_value("locked_on")
        end

        function hero:face_target()
          if game:get_value("closest_enemy") ~= nil then
            local closest_enemy = hero:get_map():get_entity(game:get_value("closest_enemy"))
--print(closest_enemy:get_name())
          else
--print("no enemies")
            return
          end
          local screen = hero:get_map():get_camera():get_surface()
          local dir = hero:get_direction4_to(closest_enemy)
          hero:set_direction(dir)

        end
        
        if closest_enemy ~= nil then
          locked_on_timer = sol.timer.start(hero, 10, function() if game:get_value("locked_on")
                                                                                             and math.abs(hero:get_distance(closest_enemy)) <= range
                                                                                             then 
                                                                                              hero:face_target()  return true
                                                                                             elseif math.abs(hero:get_distance(closest_enemy)) > range
                                                                                             then
                                                                                              game:set_value("locked_on", false)
                                                                                              locked_on = game:get_value("locked_on")
                                                                                              return false
                                                                                             end 
                                                                              end)
          line_of_sight_timer = sol.timer.start(hero, 1500, function() local los = hero:has_los(closest_enemy) if los then return true 
                                                                                             else game:set_value("locked_on", false) locked_on = game:get_value("locked_on") return false
                                                                                             end 
                                                                                    end)
        end --end if closest_enemy ~= nil for timer.
      
    end  --End if locked_on statement

    handled = true
  end  --End the if key == q and set handled to true

  return handled
end

local game_meta = sol.main.get_metatable("game")
game_meta:register_event("on_key_pressed" ,  lock_on)