local item_meta = sol.main.get_metatable("item")


local function on_obtained(item, variant, savegame_variable)
  local game = item:get_game()
  game:add_to_current_inventory(item)
end



item_meta:register_event("on_obtained", on_obtained)