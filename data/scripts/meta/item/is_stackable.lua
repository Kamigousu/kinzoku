local item_meta = sol.main.get_metatable("item")


function item_meta:set_stackable(stackable)
  if stackable == nil or (stackable ~= true and stackable ~= false) then
    item_meta.is_stackable = stackable
  else
    item_meta.is_stackable = false
  end
end
