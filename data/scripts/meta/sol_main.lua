local main_meta = sol.main.get_metatable("main")

function main_meta:range(t, f, l)
  local range = {}
  for i=f,l do
    range[#range + 1] = t[i]
  end
  return range
end
