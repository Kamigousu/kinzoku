local hero_meta = sol.main.get_metatable("hero")
require("scripts/multi_events")
--Lists of possible armours and weapons.
local armour_list = require("scripts/armours")
local weapon_list = require("scripts/weapons")
local armour_sprite
local weapon_sprite
local armour
local weapon
local strafing = require("scripts/meta/hero/strafing_state")

function hero_meta:is_ready()
  local hero = self
  local state = hero:get_state()
  local ready
  if state == "free" or (state == "custom") then
    ready = true
  else
    ready = false
  end
  return ready
end


function hero_meta:on_state_changed(new_state_name)
  local game = self:get_game()
  local state = new_state_name
  if state == "sword swinging" then
--print("Stamina before: "..game:get_stamina())
    game:set_stamina(game:get_stamina() - (25 + math.floor((math.floor(weapon.eqw) ^ 2) / 4)))
--print("ATKSPD before: "..self:get_property("attack_speed"))
--print("Stamina after: "..game:get_stamina())

    local weapon = self:get_sprite("sword")
    local frame_delay = math.ceil(game:get_value("attack_speed"))
--print("ATKSPD after: "..frame_delay)
    weapon:set_frame_delay(frame_delay)
  end
end

function hero_meta:calculate_parameters()
  local hero = self
  local game = hero:get_game()

--Define original health, focus, and stamina parameters.
  local hp = 100
  local fp = 100
  local sp = 100

--Armour and weapon sprite names, sub the file path.
  armour_sprite = hero:get_tunic_sprite_id():sub(6)
  weapon_sprite = hero:get_sword_sprite_id():sub(6)
  game:set_value("armour", armour_sprite)
  game:set_value("weapon", weapon_sprite)

--Currently equipped armour and weapon.
  armour = armour_list[armour_sprite]
  weapon = weapon_list[weapon_sprite]

--Armour properties.
  local a_mit = armour.mit
  local a_eqw = armour.eqw
  local a_bns = armour.bns


--Weapon properties.
  local w_dmg = weapon.dmg
  local w_mit = weapon.mit
  local w_spd = weapon.spd
  local w_con = weapon.con
  local w_eqw = weapon.eqw


--Determine the armour bonus, if any, and apply it to the hero via user properties.
  game:set_value("armour_bonus", a_bns)
  if a_bns == "HP+" then
    hp = hp * 1.275
    game:set_max_life(hp)
  elseif a_bns == "FCS+" then
    fp = fp * 1.275
    game:set_max_magic(fp)
  elseif a_bns == "STM+" then
    sp = sp * 1.275
    game:set_max_stamina(sp)
  else
    game:set_max_life(hp)
    game:set_max_magic(fp)
    game:set_max_stamina(sp)
  end

--Calculate the total equipweight of the hero.
  local eqw_buff = false
  local eqw_debuff = false
  local total_eqw = a_eqw + w_eqw
  if a_bns == "EQW-10%" then total_eqw = total_eqw * 0.90 
  else total_eqw = a_eqw + w_eqw 
  end
--Apply Debuffs first.
  if game:get_value("ironhide_buff") == true  and eqw_debuff == false then
    total_eqw = (total_eqw * 1.55) + 2.5
    eqw_debuff = true
  end
--Apply Buffs after Debuffs.
  if game:get_value("featherweight_buff") == true and eqw_buff == false then
    total_eqw = (total_eqw * 0.70)
    eqw_buff = true
  end
  if total_eqw <= 0 then total_eqw = 1 end
  game:set_value("equip_weight", total_eqw)

--Calculate the total movement speed of the hero.
  local spd_buff = false
  local spd_debuff = false
  local total_spd = (176 * (1 - 0.075)^(total_eqw -1)) + 24
  if a_bns == "STM++" then
    sp = sp * 1.6
    game:set_max_stamina(sp)
    total_spd = total_spd * 1.2
  elseif a_bns == "INVIS" then
    total_spd = total_spd * 1.6
  else
    sp = 100
    game:set_max_stamina(sp)
    total_spd = (176 * (1 - 0.075)^(total_eqw -1)) + 24
  end
--Apply Debuffs First. Stronger Debuffs apply first.
  if game:get_value("stonewrath_buff") == true and spd_debuff == false then
    total_spd = total_spd * 0.50
    spd_debuff = true
  end
--Apply Buffs after Debuffs. Stronger Buffs apply first.
  if (game:get_value("airlash_buff") == true or game:get_value("steelskin_buff") == true) and spd_buff == false then
    total_spd = total_spd * 1.250
    spd_buff = true
  end
  if total_spd <= 0 then total_spd = 24 end
  game:set_value("movement_speed", total_spd)

--Calculate the total weapon attack speed for the hero's main hand weapon.
  local atk_spd_buff = false
  local atk_spd_debuff = false
  local attack_spd = w_spd * (total_spd/100)
  if attack_spd <= 0 then attack_spd = w_spd end
  game:set_value("attack_speed", attack_spd)

--Determine the attack damage of the hero's weapon.
  local atk_buff = false    --use this to flag when a buff has been applied. After flag is true, all other buff will not apply.
  local atk_debuff = false
  local attack_dmg = w_dmg
  if a_bns == "FCS++" then
    fp = fp * 1.6
    game:set_max_magic(fp)
    attack_dmg = attack_dmg * 1.2
  else
    fp = 100
    game:set_max_magic(fp)
    attack_dmg = w_dmg
  end
--Apply Debuffs First. Stronger Debuffs apply first.
  if game:get_value("steelskin_buff") == true and atk_debuff == false then
    attack_dmg = attack_dmg * 0.50
    atk_debuff = true
  end
  if game:get_value("featherweight_buff") == true and atk_debuff == false then
    attack_dmg = attack_dmg * 0.60
    atk_debuff = true
  end
--Apply Buffs After Debuffs. Stronger Buffs apply first.
  if (game:get_value("airlash_buff") == true or game:get_value("stonewrath_buff") == true) and atk_buff == false then
    attack_dmg = attack_dmg * 1.250
    atk_buff = true
  end
  if game:get_value("obsidian_razor_buff") == true and atk_buff == false then
    attack_dmg = attack_dmg * 1.20
    atk_buff = true
  end
  game:set_value("attack_damage", attack_dmg)

--Determine the damage per second of the hero's weapon.
  local dps = (attack_dmg/attack_spd) * 1000
  game:set_value("dps", dps)

--Calculate the total mitigation of the hero's armour and weapon,
--as well as the combined mitigation of both armour and weapon.
  local armour_mit_buff = false
  local armour_mit_debuff = false
  local weapon_mit_buff = false
  local weapon_mit_debuff = false
  local mit_buff = false
  local mit_debuff = false
  local armour_mit = a_mit
  local weapon_mit = w_mit
  if a_bns == "HP++" then
    hp = hp * 1.6
    game:set_max_life(hp)
    armour_mit = (1 + armour_mit) * 1.2 - 1
    weapon_mit = (1 + weapon_mit) * 1.2 - 1
  else
    hp = 100
    game:set_max_life(hp)
    armour_mit = a_mit
    weapon_mit = w_mit
  end
--Apply Debuffs first. Stronger Debuffs apply first.
  if game:get_value("airlash_buff") == true and mit_debuff == false then
    armour_mit = armour_mit * 0.50
    weapon_mit = weapon_mit * 0.50
    mit_debuff = true
  end
  if game:get_value("obsidian_razor_buff") == true and weapon_mit_debuff == false then 
    weapon_mit = weapon_mit * 0.70
    weapon_mit_debuff = true
  end
--Apply Buffs After Debuffs. Stronger Buff apply first.
  if (game:get_value("stonewrath_buff") == true or game:get_value("steelskin_buff") == true) and armour_mit_buff == false then
    armour_mit = armour_mit * 1.250
    armour_mit_buff = true
  end
  if game:get_value("ironhide_buff") == true and armour_mit_buff == false then
    armour_mit = armour_mit * 1.25
    armour_mit_buff = true
  end
  local blocking_mit = (1 + armour_mit) * (1 + weapon_mit) - 1
  game:set_value("weapon_mitigation", tostring(weapon_mit))
  game:set_value("armour_mitigation", tostring(armour_mit))
  game:set_value("blocking_mitigation", tostring(blocking_mit))

  game:set_value("weapon_constitution", w_con)

--Apply the calculated parameters to the hero.
  hero:set_weight(total_eqw)
  hero:set_walking_speed(total_spd)
end

--Args: weapon is a string of the weapons name that you want to equip.
function hero_meta:equip_main_hand_weapon(weapon)
  local hero = self
  local game = hero:get_game()
  
  --Change the hero weapon sprite to change the equipped weapon.
  hero:set_sword_sprite_id("hero/"..weapon)
  --Save the weapon to the hero's properties.
  game:set_value("equipped_weapon", weapon)

  hero:calculate_parameters()
end

local function on_taking_damage(hero, damage)
  local game = hero:get_game()

  local a_mit = tonumber(game:get_value("armour_mitigation"))
  local w_mit = tonumber(game:get_value("weapon_mitigation"))
  local mit_damage
  --if hero:is_blocking() then
    mit_damage = damage * (1 - a_mit)
  --else
    --total_mit = (1 + a_mit) * (1 + w_mit) - 1
    --mit_damage = damage * total_mit
  --end


  hero:set_blinking(false)
  hero:set_invincible(false)
  hero:set_animation("hurt")
  game:remove_life(mit_damage)
end

local function on_created(hero)
  local game = hero:get_game()
  local map = hero:get_map()
  local initial_creation = game:get_value("initial_creation")

  sol.timer.start(hero, 10, function() if hero:get_state() == "free" then print("changing state") hero:start_state(strafing)  end return true end)
  
  if initial_creation then
    local initial_armour = game:get_value("initial_armour")
    local initial_weapon = game:get_value("initial_weapon")
    hero:set_tunic_sprite_id("hero/"..initial_armour)
    game:set_value("equipped_armour", initial_armour)
    hero:set_sword_sprite_id("hero/"..initial_weapon)
    game:set_value("equipped_weapon", initial_weapon)
    game:set_value("initial_creation", false)   --Tell the function that this is not the first time the hero is being created.
  else
    hero:set_tunic_sprite_id("hero/"..game:get_value("equipped_armour"))
    hero:set_sword_sprite_id("hero/"..game:get_value("equipped_weapon"))
  end

--Everytime the hero is created, his parameters are calculated.
  hero:calculate_parameters()

end

--Strafing function here. Requires strafing state scripts to function.
local function on_movement_changed(hero, movement)
  local hero_dir = hero:get_direction()
  local move_dir = movement:get_direction4()
  local current_anim = hero:get_animation()
  local animation

  if hero:get_state_object() == strafing and movement:get_speed() > 0 then
    if move_dir == hero_dir then 
      animation = "walking"  
    elseif math.abs(move_dir - hero_dir) == 1 then
      animation = "strafing_perp"
    elseif math.abs(move_dir - hero_dir) == 2 then
      animation = "strafing_back"
    end

  elseif movement:get_speed() <= 0 then
    animation = "stopped"
  end
    
  if current_anim == nil or current_anim ~= animation and animation ~= nil then
    hero:set_animation(animation)
  end
end

hero_meta:register_event("on_created", on_created)
hero_meta:register_event("on_taking_damage", on_taking_damage)
hero_meta:register_event("on_movement_changed", on_movement_changed)