local enemy_meta = sol.main.get_metatable("enemy")


function enemy_meta:get_loot(tier, floor)
  local loot
  local treasure
  local variant
  local roll
  local tier = tier
  local floor = floor
  local t_loot = {"a128", "b131", "c134", "d137", "e140", "f143", "g146",
                  "h149", "i152", "j155", "k158", "l161", "m164", "n167", "o170",
                  "p173", "q176", "r179", "s182", "t185",}
  if floor == nil then
    floor = 1
  end
  if tier == nil then
    tier = 1
  end
  if floor > 6 then
    floor = 6
  end

  if tier > 5 then
    tier = 5
  end

  if floor ~= 6 and tier > 4 then
    tier = 4
  end

  if floor == 1 then
    t_loot = sol.main:range(t_loot, 1, 3)
  end
  if floor == 2 then
    t_loot = sol.main:range(t_loot, 4, 7)
  end
  if floor == 3 then
    t_loot = sol.main:range(t_loot, 8, 11)
  end
  if floor == 4 then
    t_loot = sol.main:range(t_loot, 12, 15)
  end
  if floor == 5 then
    t_loot = sol.main:range(t_loot, 16, 19)
  end
  if floor == 6 then
    t_loot = sol.main:range(t_loot, 20, 20)
  end

  if tier == 4 and floor == 1 then
    loot = t_loot[tier - 1]
  elseif tier == 5 and floor == 6 then
    loot = t_loot[1]
  else
    loot = t_loot[tier]
  end

  if (tier < 5 and floor == 6) or (tier == 3 and floor == 1) then
    loot = nil
  end

--print(unpack(t_loot))
--print(loot)
  
  if loot == "a128" then
     treasure = 'irons'
     variant = 1
  end
  if loot == "b131" or loot == "d137" then
     roll = math.random(100)
    if roll > 80 then
       treasure = 'irons'
       variant = 1
    else
       treasure = 'irons'
       variant = 2
    end
  end
  if loot == "c134" or loot == "j155" or loot == "n167" then
     treasure = 'irons'
     variant = 3
  end
  if loot == "e140" or loot == "h149" then
     roll = math.random(100)
    if roll > 80 then
       treasure = 'irons'
       variant = 2
    else
       treasure = 'irons'
       variant = 1
    end
  end
  if loot == "f143" or loot == "q176" then
     roll = math.random(100)
    if roll > 80 then
       treasure = 'irons'
       variant = 2
    else
       treasure = 'irons'
       variant = 3
    end
  end
  if loot == "g146" or loot == "r179" then
     roll = math.random(100)
    if roll > 80 then
       treasure = 'irons'
       variant = 3
    else
       treasure = 'irons'
       variant = 4
    end
  end
  if loot == "i152" or loot == "l161" or loot == "m164" or loot == "p173" then
     treasure = 'irons'
     variant = 2
  end
  if loot == "o170" or loot == "s182" then
     treasure = 'irons'
     variant = 4
  end
  if loot == "t185" then
     roll = math.random(100)
    if roll > 95 then
       treasure = 'irons'
       variant = 6
    else
       treasure = 'irons'
       variant = 5
    end
  end
--print(treasure.." treasure of variant "..variant)
  return treasure, variant
end