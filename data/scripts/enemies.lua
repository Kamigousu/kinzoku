local enemy_list = {
subesin = {hp=50, fp=70, sp=125, dmg=28, mit=0.20, spd=128,},
zargousu = {hp=40, fp=0, sp=175, dmg=22, mit=0.16, spd=176,},
zealot = {hp=135, fp=60, sp=120, dmg=35, mit=0.32, spd=112,},
sorcerer = {hp=55, fp=160, sp=150, dmg=65, mit=0.12, spd=96,},
knight = {hp=200, fp=45, sp=160, dmg=45, mit=0.45, spd=88,},
ghost_minion = {hp=1, fp=1, sp=1, dmg=1, mit=0, spd=1,},
}

return enemy_list