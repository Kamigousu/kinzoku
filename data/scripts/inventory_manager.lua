local inventory_manager = {}
local game = sol.main.get_game()
local storage = {}    --Storage is all of the items in the inventory_pane.
local equipment = {}    --Equipment is all of the items currently equipped in the bottom slots.



function inventory_manager:add_to_storage(item)


end


--get_equipped_weapon(hand) returns the currently equipped weapon
--in the hand you specify, possible values being 'mainhand' or 'offhand'.
function inventory_manager:get_equipped_weapon(hand)
  local hero = game:get_hero() 
  local weapon

--set the weapon based on the argument passed to the hand parameter.
  if hand == "offhand" then
--if argument passed to hand is the string "offhand" then return
--the off hand weapon (either a shield or some tool).
    weapon = hero:get_shield_sprite_id()
  else
--If the argument passed to hand is not the string "offhand" then 
--default to the mainhand weapon, usually a sword.
    weapon = hero:get_sword_sprite_id()
  end

  return weapon
end



function inventory_manager:get_equipped_armour()

end



function inventory_manager:get_equipped_item(item)

end



return inventory_manager