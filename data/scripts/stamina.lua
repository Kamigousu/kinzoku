local game_meta = sol.main.get_metatable("game")

  function game_meta:get_stamina()
    return self:get_value("stamina")
  end
  
  function game_meta:set_stamina(value)
    if value > self:get_max_stamina() then value = 
      self:get_max_stamina()
    end
    return self:set_value("stamina", value)
  end
  
  function game_meta:add_stamina(value)
    stamina = self:get_value("stamina") + value
    if value >= 0 then
      if stamina > self:get_max_stamina() then stamina =
      self:get_max_stamina()
      end
      return self:set_value("stamina", stamina)
    end
  end
  
  function game_meta:remove_stamina(value)
    stamina = self:get_value("stamina") - value
    print("stamina removed: ("..value..")")
    print("Stamina: ("..stamina..")")
    if stamina < 0 then stamina = 0 end
    if value >= 0 then
      return self:set_value("stamina", stamina)
    end
  end
  
  function game_meta:get_max_stamina()
    return self:get_value("max_stamina")
  end
  
  function game_meta:set_max_stamina(value)
    if value >= 45 then  --Minimum stamina
      return self:set_value("max_stamina", value)
    else
      return self:set_value("max_stamina", 45)
    end
  end

function game_meta:get_stamina_displayed()
  local stam_disp = self:get_stamina() / 2
  return stam_disp
end

function game_meta:set_stamina_displayed(value)
  local max_stam_disp = self:get_max_stamina_displayed()
  if value < 0 then
    value = 0
  elseif value > max_stam_disp then
    value = max_stam_disp
  end
  local stam_disp = value
  return stam_disp
end

function game_meta:get_max_stamina_displayed()
  local max_stam = self:get_max_stamina()
  local max_stam_disp = max_stam / 2
  return max_stam_disp
end

function game_meta:is_stamina_regen()
  local hero = self:get_hero()
  local state = hero:get_state()
  if state == "sword swinging"
  or state == "sword loading"
  or state == "sword spin attack"
  or self:is_hero_running() == true
  or self:is_hero_rolling() == true
  or self:is_hero_exhausted() == true then
    return false
  else
    return true
  end
end

function game_meta:is_hero_running()
  local game = self
  if game:get_stamina() <= 0 then
    sol.input.simulate_key_released("left shift")
  end
  if sol.input.is_key_pressed("left shift") == true
  and (sol.input.is_key_pressed("up") == true
  or sol.input.is_key_pressed("down") == true
  or sol.input.is_key_pressed("left") == true
  or sol.input.is_key_pressed("right") == true)
  or sol.input.is_joypad_button_pressed(8) then
print(game:get_stamina())
    return true
  elseif (sol.input.is_key_pressed("left shift") == false and not sol.input.is_joypad_button_pressed(8))
  or(sol.input.is_key_pressed("up") == false
  and sol.input.is_key_pressed("down") == false
  and sol.input.is_key_pressed("left") == false
  and sol.input.is_key_pressed("right") == false)
  or game:get_stamina() <= 0 then
    return false
  end
end
  
function game_meta:is_hero_rolling()
  local game = self
  if game:get_value("rolling") == true
  and game:get_stamina() > 0 then
    return true
  elseif game:get_value("rolling") == false
  --or game:get_stamina() <= 0 
  then
    return false
  end
end

function game_meta:is_hero_exhausted()
  local game = self
  local stamina = game:get_stamina()
  if stamina >= 0 then
    return false
  elseif stamina < 0 then
    return true
  end
end

return game_meta