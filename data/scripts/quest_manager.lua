local quest_manager = {}

function quest_manager:initialise_game()
  require("scripts/stamina")
print("Stamina initialised!")
  require("scripts/focus")
print("Focus initialised!")
end

function quest_manager:initialise_hero()
  --sol.timer.start(1500, function() require("scripts/aim")return false end)
end
function quest_manager:initialise_quest()
  quest_manager:initialise_game()
  quest_manager:initialise_hero()
print("Game initialised!")
end

return quest_manager